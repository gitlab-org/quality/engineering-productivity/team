# RUNBOOK - Predictive Test Selection

This documentation is the next read after [the GitLab pipelines development documentation](https://docs.gitlab.com/ee/development/pipelines/index.html#predictive-test-jobs-before-a-merge-request-is-approved) if you'd like to understand the Predictive Test Selection system set up in `gitlab-org/gitlab`.

[[_TOC_]]

## Production Incidents

### xxx

## Maintenance tasks

### Discover Predictive Test Selection gaps

#### Context

We measure backend predictive test selection accuracy in [this dashboard](https://app.snowflake.com/ys68254/gitlab/w2xuMwRtSBVD/chart) (and [the table version](https://app.snowflake.com/ys68254/gitlab/w2NRGUe1t4ID)).

We look at backend predictive pipelines that were successful, followed by a full pipeline that failed.

The problem with this approach is that the full pipeline failing could be due to other reasons than a test selection gap: flaky test, infrastructure issue, ...

In this maintenance task, we want to discover predictive test selection gaps and create issues to fix them.

#### Instructions

* [Create an issue for the analysis](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/new?issuable_template=Discover%20predictive%20test%20selection%20gaps) with the `Discover predictive test selection gaps` issue template
* Open [the table version of the dashboard](https://app.snowflake.com/ys68254/gitlab/w2NRGUe1t4ID)
* [Check the previous analyses done](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/?sort=created_date&state=opened&label_name%5B%5D=maintenance%3A%3Atest-gap&first_page_size=100), and start at pipelines we haven't yet investigated.
* Go through each line in the table
  * Look at the failed full pipeline failed jobs, and try to understand whether those jobs failed due to flaky tests or infrastructure issue (see the tip below)
  * If you think the error was a test selection gap, please [create an issue in this EPIC](https://gitlab.com/groups/gitlab-org/-/epics/13392).
  * Document your findings in the issue created above.

**Tips:**

* Look at the merge request pipelines view listing all the pipelines for the merge request: it helps to understand whether the pipelines failed consistently.
* Check the commits for each merged results pipeline to see whether they share the same commit from the merge request
  * For instance in https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146708, we see that **the 2nd and 3rd pipelines share the same commit as a parent from the author** ([pipeline 2nd](https://gitlab.com/gitlab-org/gitlab/-/commit/2d7c9980ce3eea546a3f6e7237ab3f75645c3376), [pipeline 3rd](https://gitlab.com/gitlab-org/gitlab/-/commit/1e464cd1871bcb26930afe8319aec5812f469fec)). That is, both of the commit of the merged results pipeline shared `77304765` as one of the parents, which is a commit from the merge request. This means while the pipelines ran on different merged results, we can assume that the changes are the same because the likelihood that changes from the default branch affect the outcome should not be high. This is not obvious to tell until we look at the parents of the merged results commit.
* In predictive backend pipelines, check the output of the `detect-tests` job (in the `prepare` CI/CD stage) to see which specs were predicted.

## Learning

### Overview

If you haven't done so already, see the overview at [the GitLab pipelines development documentation](https://docs.gitlab.com/ee/development/pipelines/index.html#predictive-test-jobs-before-a-merge-request-is-approved).

We use [dynamic mappings](https://docs.gitlab.com/ee/development/pipelines/#dynamic-mappings) and [static mappings](https://docs.gitlab.com/ee/development/pipelines/#static-mappings) to map changed files to tests we should run:

* Dynamic mappings
  * Crystalball mappings (e.g. [this one](https://gitlab.com/gitlab-org/gitlab/-/blob/eb79ecddf9c91fbc6c74c78b420784d8797e22da/spec/crystalball_env.rb#L22))
  * Custom mappings (e.g. [those ones](https://gitlab.com/gitlab-org/gitlab/-/blob/264fc6e1b02dc53380c3ba2f7cdcbbab82a6b546/tooling/lib/tooling/predictive_tests.rb#L45-47))
* Static mappings
  * [`tests.yml`](https://gitlab.com/gitlab-org/gitlab/-/blob/a9244d00c5efb78048928166c4abf3e6da75b086/tests.yml)

We generate some dynamic mappings in scheduled pipelines (Crystalball), and some others when running test detection (Custom mappings).

The [detect-tests CI job](https://gitlab.com/gitlab-org/gitlab/-/blob/5be9353de85fb37b74103f07dd153f714186d6da/.gitlab/ci/setup.gitlab-ci.yml#L135) is where we compile the test predictions from dymamic/static mappings for a given merge request.

### TestFileFinder

We use [the `test_file_finder` gem](https://gitlab.com/gitlab-org/ruby/gems/test_file_finder) as the primary interface for Predictive Test Selection. Its main job is to **concatenate predictions from different sources**:

* [Use dynamic mappings from Crystalball](https://gitlab.com/gitlab-org/gitlab/-/blob/264fc6e1b02dc53380c3ba2f7cdcbbab82a6b546/tooling/lib/tooling/find_tests.rb#L19-21)
* [Use static mappings](https://gitlab.com/gitlab-org/gitlab/-/blob/264fc6e1b02dc53380c3ba2f7cdcbbab82a6b546/tooling/lib/tooling/find_tests.rb#L17) from [the `tests.yml` file](https://gitlab.com/gitlab-org/gitlab/-/blob/a9244d00c5efb78048928166c4abf3e6da75b086/tests.yml).

### Crystalball

#### Overview

The `crystalball` gem has three parts:

* [Map Generators](https://toptal.github.io/crystalball/map_generators/)
* [Predictors](https://toptal.github.io/crystalball/predictors/)
* [Runner](https://toptal.github.io/crystalball/runner/)

At the moment, we're only using the **Map Generators**, as the `Predictors`/`Runners` would mainly be used locally.

Crystalball is generating **JSON maps** that we use to predict which tests we should run. In this map, the keys are **file names**, and the values are **the tests we should run if that file changed**:

```json
  "app/services/users/destroy_service.rb": [
    "ee/spec/services/ee/users/destroy_service_spec.rb",
    "spec/services/users/destroy_service_spec.rb"
  ],
  "ee/app/services/security/ingestion/mark_as_resolved_service.rb": [
    "ee/spec/services/security/ingestion/mark_as_resolved_service_spec.rb"
  ],
  "ee/lib/gitlab/ci/reports/license_scanning/report.rb": [
    "ee/spec/lib/gitlab/ci/reports/license_scanning/report_spec.rb",
    "spec/lib/gitlab_spec.rb"
  ],
  [...]
```

You can choose how to generate this mapping based on [**Strategies**](https://toptal.github.io/crystalball/map_generators/).

#### Strategies

There are a few of them:

* [CoverageStrategy](https://toptal.github.io/crystalball/map_generators/#coveragestrategy)
* [AllocatedObjectsStrategy](https://toptal.github.io/crystalball/map_generators/#allocatedobjectsstrategy)
* [DescribedClassStrategy](https://toptal.github.io/crystalball/map_generators/#describedclassstrategy)
* [ParserStrategy](https://toptal.github.io/crystalball/map_generators/#parserstrategy)
* [ActionViewStrategy](https://toptal.github.io/crystalball/map_generators/#actionviewstrategy)
* [I18nStrategy](https://toptal.github.io/crystalball/map_generators/#i18nstrategy)
* [FactoryBotStrategy](https://toptal.github.io/crystalball/map_generators/#factorybotstrategy)
* [Custom strategies](https://toptal.github.io/crystalball/map_generators/#custom-strategies)

They can also be **combined**. We used the **Coverage strategy** since we implemented Crystalball in our pipelines. We did an investigation in https://gitlab.com/gitlab-org/gitlab/-/issues/413510 to see which strategies we could use instead/in addition.

To see which strategies are in use, have a look at https://gitlab.com/gitlab-org/gitlab/-/blob/master/spec/crystalball_env.rb.

#### How do we use Crystalball in the `gitlab-org/gitlab` pipelines?

Most of the configuration is in https://gitlab.com/gitlab-org/gitlab/-/blob/master/spec/crystalball_env.rb:

* We control when the JSON mappings are created with [the `CRYSTALBALL` ENV variable](https://gitlab.com/gitlab-org/gitlab/-/blob/cc0c54cb9e3f47a567b4fbc1ea018d2fbdd30f11/spec/crystalball_env.rb#L9)
* We create the JSON mappings [in the `crystalball/` folders for all RSpec jobs](https://gitlab.com/gitlab-org/gitlab/-/blob/90b675d480825d6f496af9b73bb090ba8c32ea3a/spec/crystalball_env.rb#L15-16)

In [scheduled pipelines](https://gitlab.com/gitlab-org/gitlab/-/pipelines?page=1&scope=all&source=schedule&ref=master), we will do the following:

* Set `CRYSTALBALL=true`
* Run RSpec tests (which will create maps in the `crystalball/` folders for the RSpec CI jobs)
* We [export the local mappings as artifacts](https://gitlab.com/gitlab-org/gitlab/-/blob/e1e9ac4a4c39f745631009171d0b0b141b46f1d9/.gitlab/ci/rails/shared.gitlab-ci.yml#L164)
* Merge/compress those mappings in [the `update-tests-metadata` CI job](https://gitlab.com/gitlab-org/gitlab/-/blob/90b675d480825d6f496af9b73bb090ba8c32ea3a/.gitlab/ci/test-metadata.gitlab-ci.yml#L54), specifically [the `update_tests_mapping` shell function](https://gitlab.com/gitlab-org/gitlab/-/blob/90b675d480825d6f496af9b73bb090ba8c32ea3a/scripts/rspec_helpers.sh#L74-84)
* We [export the final compressed mapping file as artifacts](https://gitlab.com/gitlab-org/gitlab/-/blob/b2598f89f034fd9034687f1aa4657dddcabd62a8/.gitlab/ci/test-metadata.gitlab-ci.yml#L10)
* We make the final mapping available in GitLab Pages via [the `pages` CI job](https://gitlab.com/gitlab-org/gitlab/-/blob/f00a8ed00669aa2cad24ef53e7a2b4eef38c5e4a/.gitlab/ci/pages.gitlab-ci.yml#L31) (e.g. [pipeline](https://gitlab.com/gitlab-org/gitlab/-/pipelines/1214366292) | [`update-tests-metadata` job](https://gitlab.com/gitlab-org/gitlab/-/jobs/6399246625) | [`pages` job](https://gitlab.com/gitlab-org/gitlab/-/jobs/6399246636))

In MR pipelines, we will:

* If the pipeline runs RSpec Predictive tests, we will run [the `detect-tests` CI job](https://gitlab.com/gitlab-org/gitlab/-/blob/5be9353de85fb37b74103f07dd153f714186d6da/.gitlab/ci/setup.gitlab-ci.yml#L135).
* The `detect-tests` CI job will [retrieve the mapping from GitLab pages](https://gitlab.com/gitlab-org/gitlab/-/blob/5be9353de85fb37b74103f07dd153f714186d6da/.gitlab/ci/setup.gitlab-ci.yml#L129)
* It will then [use it to make dynamic predictions](https://gitlab.com/gitlab-org/gitlab/-/blob/264fc6e1b02dc53380c3ba2f7cdcbbab82a6b546/tooling/lib/tooling/find_tests.rb#L19-21).


### Run test predictions locally

We don't yet have an automated way to do so, it will require some copy/pasting.

#### Generate test predictions like `detect-tests` would

```shell
source scripts/utils.sh
source scripts/rspec_helpers.sh

# Download/extract crystalball mapping from GitLab pages
export RSPEC_PACKED_TESTS_MAPPING_PATH=crystalball/packed-mapping.json
export RSPEC_TESTS_MAPPING_PATH=crystalball/mapping.json
retrieve_tests_mapping

# Retrieve frontend mappings
export FRONTEND_FIXTURES_MAPPING_PATH=crystalball/frontend_fixtures_mapping.json
retrieve_frontend_fixtures_mapping

# Create rspec/ folder to store input/outputs from the detect-tests job
mkdir -p rspec

# detect-tests main input (files changed in the MR) and main outputs (tests that we should run by edition)
export RSPEC_CHANGED_FILES_PATH=rspec/changed_files.txt
export RSPEC_MATCHING_TESTS_PATH=rspec/matching_tests.txt
export RSPEC_MATCHING_TESTS_EE_PATH=rspec/matching_tests-ee.txt
export RSPEC_MATCHING_TESTS_FOSS_PATH=rspec/matching_tests-foss.txt

# Configuration for dynamic custom mappings
export RSPEC_MATCHING_JS_FILES_PATH=rspec/js_matching_files.txt
export RSPEC_VIEWS_INCLUDING_PARTIALS_PATH=rspec/views_including_partials.txt

# Use Crystalball dynamic mapping when detecting tests
export RSPEC_TESTS_MAPPING_ENABLED=true

export CI_API_V4_URL=https://gitlab.com/api/v4
export CI_MERGE_REQUEST_PROJECT_PATH=gitlab-org/gitlab
export CI_MERGE_REQUEST_IID=147164 # TODO: Change to the MR you'd like to replicate
export PROJECT_TOKEN_FOR_CI_SCRIPTS_API_USAGE="${GITLAB_API_PRIVATE_TOKEN}" # TODO: Ensure you have GITLAB_API_PRIVATE_TOKEN set

bundle exec tooling/bin/predictive_tests

filter_rspec_matched_foss_tests ${RSPEC_MATCHING_TESTS_PATH} ${RSPEC_MATCHING_TESTS_FOSS_PATH};
filter_rspec_matched_ee_tests ${RSPEC_MATCHING_TESTS_PATH} ${RSPEC_MATCHING_TESTS_EE_PATH};

echoinfo "Changed files: $(cat $RSPEC_CHANGED_FILES_PATH)";
echoinfo "Related FOSS RSpec tests: $(cat $RSPEC_MATCHING_TESTS_FOSS_PATH)";
echoinfo "Related EE RSpec tests: $(cat $RSPEC_MATCHING_TESTS_EE_PATH)";
echoinfo "Related JS files: $(cat $RSPEC_MATCHING_JS_FILES_PATH)";
```

#### Explore Crystalball mappings

```shell
source scripts/utils.sh
source scripts/rspec_helpers.sh

# Download/extract crystalball mapping from GitLab pages
export RSPEC_PACKED_TESTS_MAPPING_PATH=crystalball/packed-mapping.json
export RSPEC_TESTS_MAPPING_PATH=crystalball/mapping.json
retrieve_tests_mapping

# Check that the mapping is present
ls -lh crystalball/mapping.json
```

You can now explore this mapping in several ways (your IDE might have issues opening it, as the file is pretty big):

**Ruby console:**

```ruby
irb

require 'json'; mapping = JSON.parse(File.read(File.expand_path(ENV['RSPEC_TESTS_MAPPING_PATH']))); nil

# Get the first 5 keys
mapping.keys.first(5)

# Get two random entries
mapping.slice(*mapping.keys.sample(2))

# Get specific entries
files=%w[ee/app/models/ee/member.rb app/models/namespace.rb]
mapping.slice(*files)

# Get the number of specs for given keys
files=%w[ee/app/models/ee/member.rb app/models/namespace.rb]
mapping.slice(*files).map { |key, values| [key, values.count] }.to_h
```

**Shell:**

```shell
# Check the mapping with the number of occurrences:
cat crystalball/mapping.json | jq 'to_entries | map({name: .key, count: (.value | length)}) | sort_by(.count) | reverse'

# Check a specific mapping for a single file
cat crystalball/mapping.json | jq '."vendor/gems/sidekiq-7.1.6/lib/sidekiq/testing.rb"'

# Check a specific mapping count for a single file
cat crystalball/mapping.json | jq '."vendor/gems/sidekiq-7.1.6/lib/sidekiq/testing.rb" | length'
```

#### Work with Crystalball mappings locally

When run locally, crystalball [will store the local mapping in `crystalball/crystalball_data.yml`](https://gitlab.com/gitlab-org/gitlab/-/blob/eb79ecddf9c91fbc6c74c78b420784d8797e22da/spec/crystalball_env.rb#L15).

```shell
CRYSTALBALL=true be rspec spec/graphql/types/project_member_relation_enum_spec.rb

# Open the raw crystalball mapping
cat crystalball/crystalball_data.yml

# Transform it to match our usual mappings
scripts/generate-test-mapping crystalball/local_mapping.yml crystalball/crystalball_data.yml

cat crystalball/local_mapping.yml
```
