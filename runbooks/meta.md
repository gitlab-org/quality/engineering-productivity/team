# RUNBOOK - Engineering Productivity Meta

This RUNBOOK is for tasks related to the Engineering Productivity team.

[[_TOC_]]

## Maintenance tasks

### Go through the weekly EP Triage Report

#### Instructions

* Pick the latest [weekly EP Triage Report](https://gitlab.com/gitlab-org/quality/triage-reports/-/issues/?sort=created_date&state=opened&label_name%5B%5D=triage%20report&label_name%5B%5D=Engineering%20Productivity&first_page_size=100). Go through all issues in the report, and ensure the following:
  * The issue is indeed for ~"Engineering Productivity". Sometimes, automation will wrongly label the issue as ~"Engineering Productivity". Remove the label and add the appropriate group/team label instead.
  * The issue has a milestone (default: %Backlog ), an EPIC (see [our roadmap](https://gitlab.com/groups/gitlab-org/-/epics/12149)), a type label, and a priority label.
