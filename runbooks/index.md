# Engineering Productivity RUNBOOKS

[[_TOC_]]

## Purpose

The runbooks are the Single Source Of Truth (SSOT) for anything related to the Engineering Productivity team responsibilities.

This is where we document, amongst other things, the incidents we see as part of the Engineering Productivity team, and how to manage them.

## Before you start

Make sure that tooling is [setup locally](../local_setup.md).

## Runbooks

* [Engineering Productivity Infrastructure](ep-infra.md)
* [GDK](gdk.md)
* [Master Broken](master-broken.md)
* [Meta](meta.md)
* [Pipelines](pipelines.md)
* [Predictive Test Selection](predictive-test-selection.md)
* [Review apps](review-apps.md)
* [Rotating credentials](rotating-credentials.md)
* [Triage Ops](triage-ops.md)

## Unknown problems

Please [add the incident and how you investigated it to one of our runbooks](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/new?issue[title]=[Runbook]%20Add%20xxx%20)!
