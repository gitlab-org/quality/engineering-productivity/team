# RUNBOOK - Pipelines

This RUNBOOK is for production issues in the https://gitlab.com/gitlab-org/gitlab/ CI/CD pipelines.

[[_TOC_]]

## Production Incidents

### Pipeline duration recently increased

#### The problem

[Pipelines Durations](https://app.periscopedata.com/app/gitlab/858266/GitLab-Pipeline-Durations?widget=11616413&udv=1914895) increased, and we'd like to know a few things:

1. Why that's the case
1. How we could make it go down

#### How to diagnose

* Go see the [Pipelines Durations](https://app.periscopedata.com/app/gitlab/858266/GitLab-Pipeline-Durations?widget=11616413&udv=1914895)
* Refresh the widget to get the latest data available

#### How to fix it

Here's the overview of the process:

1. Sample a few recent pipelines that were above a certain duration (e.g. above 45min)
1. Trace them using `tp` locally
1. Examine the jobs in the trace that took longer than expected
1. Examine the jobs more closely to understand why they spent more time than expected


**Step 1: Sample pipelines**

There is [a list of the pipelines for the current month that were above 45min](https://app.periscopedata.com/app/gitlab/858266/GitLab-Pipeline-Durations?widget=17521109&udv=1914895). You can use this as a starting point.

pro-tip: Pipelines that were around the duration you are looking for could help (e.g. don't take pipelines that took 300 minutes if you want to understand why the average is currently at 50 minutes).

**Step 2: trace them with `tp`**

[See the dedicated section on `tp`](#trace-gitlab-orggitlab-pipelines).

**Step 3: Identify the jobs on the critical path**

Try to understand where the pipeline spent its time. Here are a few pointers:

- Did a job take too long to start?
- Did one job take much longer than other jobs? (e.g. a longer RSpec job)
- Was a job triggered after the pipeline finished?

**Step 4: Look at the problematic jobs**

Here's an example to illustrate the thought process: https://gitlab.com/gitlab-org/gitlab/-/pipelines/1090600528

This pipeline took **69 minutes** to run. Looking at its trace, we see that [an RSpec job took close to 57 minutes](https://gitlab.com/gitlab-org/gitlab/-/jobs/5651404741):

![pipeline-tracing-job-outlier](../assets/pipeline-tracing-job-outlier.png)

We see that [the RSpec process of this job took 52 minutes](https://gitlab.com/gitlab-org/gitlab/-/jobs/5651404741#L5633):

```shell
Finished in 52 minutes 0 seconds (files took 58.84 seconds to load)
2649 examples, 0 failures, 1 pending
Randomized with seed 6761
```

[Looking at Knapsack report to see how long specs took](https://gitlab.com/gitlab-org/gitlab/-/jobs/5651404741#L5351), we can see that **the slowest spec file took more than 42 minutes to run**, and **the second slowest took 14 seconds**:

```shell
Knapsack report was generated. Preview:
{
  "spec/models/application_setting_spec.rb": 2573.3182126129996,
  "spec/graphql/mutations/release_asset_links/update_spec.rb": 14.529550453999946,
  [...]
}
```

We then found out the root cause for this long-running spec file in https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/323#note_1681771999.

## Maintenance tasks

### Verify EP pipelines dashboards

#### Context

We rely on pipelines like [Merge Request pipeline duration](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#merge-request-pipeline-duration) to set our OKRs.

We need to ensure that those dashboards are accurate. If they aren't, we need to fix them.

#### Instructions

* [Create an issue](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/new?issuable_template=Verify%20EP%20pipelines%20dashboards) with the `Verify EP pipelines dashboards` template to record the results of your analysis.
* Please follow the issue template for detailed instructions on how to verify EP dashboards.

To verify the data, we'll use the `scripts/pipelines-select` script in this project:

```shell
# Fetch all pipelines for a day
$ scripts/pipelines-select --after "2024-04-17 00:00:01" --before "2024-04-17 23:59:59"

# Fetch all pipelines for a week
$ scripts/pipelines-select --after "2024-04-15 00:00:01" --before "2024-04-21 23:59:59"

# Fetch all pipelines for a month
$ scripts/pipelines-select --after "2024-03-01 00:00:01" --before "2024-03-31 23:59:59"

# Fetch all pipelines for a day, with their types
$ scripts/pipelines-select --after "2024-03-01 00:00:01" --before "2024-03-01 23:59:59" --with-types
```

##### What if you don't have the same data?

For pipeline durations, check that the script is doing what you expect it to. If it looks good, check the dashboard code to spot any issues.

For pipeline types, compare the jobs the script is using to determine a pipeline type with what we use in Snowflake. The two lists should be **identical**.

### Go through ~"flakiness::1" tests that were not quarantined

#### Context

We automatically create merge requests to quarantine very flaky tests. This automation isn't creating those merge requests in some cases.

The goal of this process is two-fold:

1. Create the missing quarantine MRs for those very flaky tests
1. Think about how we could improve the automation to create those MRs automatically in the future.

#### Instructions

* Go through ~"flakiness::1" [tests that were not quarantined](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=flakiness%3A%3A1&not%5Blabel_name%5D%5B%5D=quarantine&first_page_size=100), and create quarantine MRs manually.
  * Add the appropriate group/team labels.
  * Please take time to think about and create an issue on how we could improve the automation, so that those tests have [their quarantine MRs automatically created](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/flaky-tests-management-and-processes/#flaky-tests-management-process).

### Review the slowest tests/test files in rspec_profiling_stats

#### Context

Slow tests can impact every CI/CD pipeline, and potentially break parallelization.

#### Instructions

* Have a look at the data in https://gitlab-org.gitlab.io/rspec_profiling_stats
* When you found a test that would need to be looked at:
  * [Search for an existing issue](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=rspec%20profiling&not%5Blabel_name%5D%5B%5D=automation%3Abot-authored&first_page_size=100)
  * If there's no issue, click the **Create an Issue** link (scroll right in the tables!), and add the appropriate labels for the group to handle.
  * If there is an existing issue, but it wasn't looked at recently, consider the following actions:
    * Bump severity
    * Ping the Engineering Manager in the issue or in Slack
    * Create a quarantine MR, and assign to an Engineer from the group
    * Quarantine by default after 5 days if you don't get any replies

## Learning

### Trace `gitlab-org/gitlab` pipelines

#### Context

Tracing pipelines can be a valuable tool when trying to analyze pipelines duration.

`scripts/trace-pipeline` is the wrapper script for the `trace:pipeline_url` rake task from the [release-tools](https://gitlab.com/gitlab-org/release-tools) project.

You don't have to set up the script locally, you can just [start a new pipelines](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/pipelines/new)
with the `TRACING_PIPELINE_URL` variable set the value of the pipeline URL you want to trace. The tracing page is displayed at the end of the `trace-pipeline` job.

### Visualize Knapsack reports

#### Purpose

This tool helps team members investigate test performance issues by drilling down to a single job and analyzing how accurate Knapsack was at predicting test execution time. See [this discussion](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/272#note_1584628829) for examples of what the script generates.

#### Context

GitLab pipelines utilize Knapsack to predict spec file execution time and to distribute test files to each parallelized job.

See [Test Suite parallelization](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/development/pipelines/index.md?ref_type=heads#test-suite-parallelization) for details on how Knapsack updates test execution time in the reports.

Test execution time can sometimes differ significantly from the predicted time, resulting in rspec jobs taking much longer to finish than others within the same parallelization group. To understand what contributes to this pattern, we can use the script to visualize which test file took the longest and how accurate Knapsack was at predicting and allocating test files.

#### Instructions

1. Copy [the `knapsack-diff` script](../scripts/knapsack-diff) into `/usr/local/bin/knapsack-diff`, and make it executable with `chmod a+x /usr/local/bin/knapsack-diff`
2. Set the `GITLAB_API_TOKEN` ENV variable:

```shell
export GITLAB_API_TOKEN=<your gitlab api token>
```

3. find a rspec job you want to compare knapsack reports for and copy its job ID.
4. run `knapsack-diff <your job id>`
5. The above command should open up a browser showing a bar chart comparing the test file execution time between the master report and the job report. This chart only lists the 20 slowest test files in the job. It takes a couple of seconds for server to start, so if your page shows an error, refresh it and data should start loading.

## No Scheduled Pipeline Event Alerts

### About the incident

See [policy details in GCP](url-tbd).

For development, The incident is [provisioned with terraform](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/qa-resources/modules/gitlab-pipelines/alerts.tf).

### The problem

There has not been any scheduled pipeline event from gitlab master branch in the last 12 hours.

Another less likely but possible cause is gitlab-org webhook got disabled, so no events can be emitted despite the scheduled pipelines still being fully operational. See [gitlab-org webhook event stopped](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/runbooks/triage-ops.md#gitlab-org-webhook-event-stopped) for details.

### How to diagnose

Go to [Pipeline Schedules page](https://gitlab.com/gitlab-org/gitlab/-/pipeline_schedules) and check the `Nightly` and the `[2-hourly]` schedules for the master branch to confirm when the last scheduled master pipelines ran.

You can also go to [finished pipelines page](https://gitlab.com/gitlab-org/gitlab/-/pipelines?page=1&scope=finished&ref=master&source=schedule) to look for any failed scheduled pipeline attempts. It is likely that the CI configuration has errors preventing the scheduled pipeline creation.

### How to fix

See the following MRs if you need some ideas of how an error could have contributed to a broken scheduled pipeline, and what a fix might look like:
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/124870
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/123703
