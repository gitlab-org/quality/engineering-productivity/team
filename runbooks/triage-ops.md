# RUNBOOK - Triage Ops

[[_TOC_]]

## Production Incidents

You may be visiting this page because a Google Cloud Monitoring incident prompted you here. If you are new to this section, it would be best to start with [How to troubleshoot triage-ops?](#how-to-troubleshoot-triage-ops).

### https://triage-ops.gitlab.com/ is down

#### About the incident

See [policy details in GCP](https://console.cloud.google.com/monitoring/alerting/policies/15504976932846602172?project=gitlab-qa-resources).

Example:

```
Incident #0.mu8h8is5rsw9 is ongoing
Failure of uptime check for triage-ops
```

For development, The incident is [provisioned with terraform](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/qa-resources/modules/triage-reactive/alerts.tf#L33).

#### The problem

https://triage-ops.gitlab.com/ is down. See [How to diagnose section](#how-to-diagnose).

#### How to diagnose

* Go to https://triage-ops.gitlab.com/
* If triage-ops is correctly running, you should receive a JSON response with the payload `{"status": "unauthenticated"}`. If you get anything else, there's a problem.
* Additionally, you could try to access the triage-ops dashboard: https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/doc/reactive/dashboard.md

#### How to fix it

* [Check out the triage-ops pods](https://console.cloud.google.com/kubernetes/workload/overview?project=gitlab-qa-resources&pageState=(%22savedViews%22:(%22i%22:%22333864b1ed8c43308c83a14efd5500df%22,%22c%22:%5B%22gke%2Fus-central1%2Ftriage-ops-prod%22%5D,%22n%22:%5B%5D)))
* If one is failing, click on it, and check its logs
* Delete the pod via `kubectl`:

```shell
qa-resources
kubens triage-ops-7776928-production
kubectl get pods # check the triage-ops pod name
kubectl delete pod <pod-name>
```

Once triage-ops is live again, there are a few tasks that need to be done. See [Re-enable Gitlab groups webhooks](#re-enable-gitlab-groups-webhooks) and [Communicate the outage](#communicate-the-outage) sections.

#### Re-enable GitLab groups webhooks

From https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#re-enable-disabled-webhooks,

> To re-enable a failing or failed webhook, **send a test request**. If the test request succeeds, the webhook is re-enabled

* gitlab-org group: https://gitlab.com/groups/gitlab-org/-/hooks
* gitlab-com group: https://gitlab.com/groups/gitlab-com/-/hooks

(Note: Members of the Engineering Productivity team should have access to the links above, thanks to https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/1373#note_1899565432).

#### Communicate the outage

* Slack - Communicate in `#development`, `#frontend`, `#backend` about the incident:

```markdown
Hi all,

triage-ops is currently down (incident: XXX), this means that the auto-labeling in MRs won't happen until we resolve the incident. If you have any questions, let us know in #g_engineering_productivity.
```

* Discord - Post the following message in the `announcements` channel (https://discord.com/channels/778180511088640070/778198020709679106 - you will need to create an account first at https://discord.com/invite/gitlab):

```markdown
Hi all,

triage-ops is currently down (incident: XXX), which means the `@gitlab-bot` won't be available until we resolve the incident. We're working hard to fix it.
```

**Communicate the resolution of the incident in a thread**

* Slack - Create a thread to the outage announcements above, and post the following message:

```markdown
`triage-ops` is back online :tada: (incident: XXX).
```

* Discord - Reply to the outage announcement above, and post the following message:

```markdown
`triage-ops` is back online :tada: (incident: XXX).
```

### gitlab-org webhook event stopped

#### About the incident

See [policy detail in GCP](https://console.cloud.google.com/monitoring/alerting/policies/11514993998228301585?project=gitlab-qa-resources).

Example:

```
Incident #0.mub5wnze1vk0 is ongoing
Kubernetes Container - logging/user/Events-from-gitlab-org
Alert when there is no events from gitlab-org within 15 minutes
The metric logging/user/Events-from-gitlab-org for gitlab-qa-resources triage-web with metric labels {log=stdout} has not been seen for over 15 minutes.
```

For development, The incident is [provisioned with terraform](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/qa-resources/modules/triage-reactive/alerts.tf).

#### The problem

We haven't received any events from gitlab-org in the last 15 minutes. This could be caused by:

1. No user activity in GitLab. This is possible during slow hours/weekends/holidays.
2. The gitlab-org webhooks were auto-canceled because triage-ops replied with 4 consecutive HTTP 5xx responses ([see docs](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#auto-disabled-webhooks)).

#### How to fix it

1. In the first case, we can ignore the incident, which will close by it self once activity resumes.
1. In the second case, we can resolve the problem by [re-enable Gitlab groups webhooks](#re-enable-gitlab-groups-webhooks). It is also worth checking if there is an ongoing [triage-ops outage](#httpstriage-opsgitlabcom-is-down) if the test request fails again.

### gitlab-com webhook event stopped

#### About the incident

See [policy detail in GCP](https://console.cloud.google.com/monitoring/alerting/policies/12452383442654946995?project=gitlab-qa-resources).

Example:

```
Incident #0.mub5wnze1vk0 is ongoing
Kubernetes Container - logging/user/Events-from-gitlab-com
Alert when there is no events from gitlab-com within 15 minutes
The metric logging/user/Events-from-gitlab-com for gitlab-qa-resources triage-web with metric labels {log=stdout} has not been seen for over 15 minutes.
```

For development, The incident is [provisioned with terraform](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/qa-resources/modules/triage-reactive/alerts.tf).

#### The problem

We haven't received any events from gitlab-com in the last 15 minutes. This could be caused by:

1. No user activity in GitLab. This is possible during slow hours/weekends/holidays.
2. The gitlab-com webhooks were auto-canceled because triage-ops replied with 4 consecutive HTTP 5xx responses ([see docs](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#auto-disabled-webhooks)).

#### How to fix it

1. In the first case, we can ignore the incident, which will close by it self once activity resumes.
1. In the second case, we can resolve the problem by [re-enable Gitlab groups webhooks](#re-enable-gitlab-groups-webhooks). It is also worth checking if there is an ongoing [triage-ops outage](#httpstriage-opsgitlabcom-is-down) if the test request fails again.

## Learning

### How to troubleshoot triage-ops?

See [this discussion](https://gitlab.com/gitlab-org/quality/contributor-success/team-task/-/issues/129#note_1217155426) for some inspiration. Below are the generic advice from that thread:

* Check the triage-ops logs with any useful data you have ([GCP logs query](https://cloudlogging.app.goo.gl/X2PvStBH42gFMjCU9))
  * To see which processors were chosen for a particular request, you can search for the `.jsonPayload.results` array that contains the decision from triage-ops about which processors it should use (i.e. which ones are applicable).
  * Investigate the data in `jsonPayload.event_payload.object_attributes`. Please note that we publish up time check events regularly, which we can see in the logs as `IssueEvent` with an empty `object_attributes`. Do not confuse these with the actual webhook events! See https://gitlab.com/gitlab-org/gitlab/-/issues/414765#note_1423140359 for details.
* Test triage-ops locally
  * Until we have a console available locally, you'll have to do the following steps:

```shell
bundle exec --gemfile triage/Gemfile irb
```

```ruby
irb(main):002:0> require_relative 'triage/triage'
=> true
irb(main):003:0> Triage.cache.get('Triage::PeriscopeCsv')
=> nil
```

* If nothing worked, consider restarting triage-ops, by killing the pod in the Kubernetes cluster:

```shell
qa-resources
kubens triage-ops-7776928-production
kubectl get po # Copy the pod name
kubectl delete po <pod-name>

# To verify that triage-ops restarted successfully
kubectl get pods # should be in "Running" state
kubectl logs -f <pod-name>
```

### How to do a one-off labels migration?

Check out the [One-off policies in triage-ops documentation](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/doc/scheduled/index.md#one-off-policies).

### Add a new processor chart to the GCP Processors dashboard

Processor [metrics](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/qa-resources/modules/triage-reactive/metrics.tf)
and [monitoring dashboards](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/qa-resources/modules/triage-reactive/processors_dashboard.tf)
are managed through Terraform.

#### Add a new processor metric

Duplicate an existing processor metric in [`qa-resources/modules/triage-reactive/metrics.tf`](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/qa-resources/modules/triage-reactive/metrics.tf)
and replace the duplicated processor name with the one you want to track.

For instance, if your processor class is `MyProcessorClass`, you would create this new metric:

```json
resource "google_logging_metric" "processor_events-MyProcessorClass" {
  description = "MyProcessorClass calls"
  filter      = <<-EOT
    resource.type="k8s_container"
    labels.k8s-pod/app="triage-web"
    jsonPayload.results.processor="Triage::MyProcessorClass"
  EOT
  name        = "processor_events/MyProcessorClass"

  metric_descriptor {
    metric_kind = "DELTA"
    unit        = "1"
    value_type  = "INT64"
  }
}
```

#### Add a new chart in the Processors dashboard

The simpler is to add the chart at the end of the dashboard definition in
[`qa-resources/modules/triage-reactive/processors_dashboard.tf`](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/qa-resources/modules/triage-reactive/processors_dashboard.tf).

Similarly to metrics, you can duplicate the last chart definition (don't forget to add a `,` between the two charts so that the JSON is valid), and replace the duplicated processor name
(in the `widget.title` and `widget.dataSets[0].timeSeriesQuery.timeSeriesFilter.filter` keys) with the one you want to chart.

Please be careful with the `xPos` and `yPos` keys. The dashboard is configured as a 4 columns grid with each chart having a width of 3 and a height of 4.

- The first chart in a row should not have a `xPos` key at all (otherwise GCP complains about the useless default value), then the next chart should have `"xPos": 3`, then `"xPos": 6`, and finally `"xPos": 9`.
- For `yPos`, this is similar, but it increases by 4, and only after a row is complete. All the charts in the same row should have the same `yPos`.

If we give each chart the index they have in the JSON definition they would be represented as follow in the dashboard grid:

|   | No `xPos` | `"xPos": 3` | `"xPos": 6` | `"xPos": 9` |
| - | --------- | ---------- | ----------- | ----------- |
| No `yPos` | 0 | 1 | 2 | 3 |
| `"yPos": 4`| 4 | 5 | 6 | 7 |
| `"yPos": 8`| 8 | 9 | 10 | 11 |

And then the JSON array under the `mosaicLayout.tiles` key would be:

```json
[
  { Chart 0 },
  { Chart 1 },
  { Chart 2 },
  { Chart 3 },
  { Chart 4 },
  { Chart 5 },
  { Chart 6 },
  { Chart 7 },
  { Chart 8 },
  { Chart 9 },
  { Chart 10 },
  { Chart 11 }
]
```
#### Add new environment variables

If adding environment variables for reactive processors, please follow these steps:
1. Add this variable to the [CI/CD settings](https://gitlab.com/gitlab-org/quality/triage-ops/-/settings/ci_cd) under the `Variables` section.
2. create a [Kubernetes Secret](https://kubernetes.io/docs/concepts/configuration/secret/) by updating [`.gitlab/ci/triage-web.yml`](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/.gitlab/ci/triage-web.yml#L62)
3. update [`config/triage-web.yaml`](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/config/triage-web.yaml#L27) to ensure the variable is available to triage-ops after deploy. 

For reference, you can search for `gitlab-webhook-token` in the repo to see how it is configured.
