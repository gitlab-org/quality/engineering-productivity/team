# RUNBOOK - Review apps

[[_TOC_]]

## Production incidents

### Communication to the company

* Write in the `#development` channel, and forward the message to the `#quality` and `#g_engineering_productivity` channels afterwards. Below is a template to be adapted to your needs:

```markdown
:mega: gitlab-org/gitlab review-apps deployments are currently broken (see https://gitlab.com/gitlab-org/quality/engineering-productivity/review-apps-broken-incidents/-/issues/XXX). A fix is on its way: XXX. We'll reply in this thread once the incident is resolved.
```

* When resolved, comment in the main thread, and **send to the channel**:

```markdown
:mega: review-apps are back (see RCA)
```

* If we need to **disable review-apps**, comment in the main thread, and **send to the channel**:

```shell
:mega: We are still searching for the root cause. To prevent new pipelines for failing, we'll disable review apps until we have fixed the issue.
```

To disable review-apps, please go to [Temporarily disable review-apps](#temporarily-disable-review-apps).

### General troubleshooting steps

#### The problem

Some `review-deploy` jobs are failing in the CI.

#### How to diagnose

Important: If several review app deployments are failing, please [Communicate in Slack that the review-apps are currently broken](#communication-to-the-company)

##### From outside the Kubernetes cluster

* Find a review app URL in a failed `review-deploy` CI job (hint: search for `gitlab-review`), for instance https://gitlab-review-331432-per-3db6b1.gitlab-review.app/, and curl the domain name in verbose mode:

```shell
curl -v <REVIEW_APP_URL>
```

You should then see if there is an issue with the DNS, TLS handshake, or HTTP error codes coming from the review app. Read on to continue the troubleshooting.


* If it looks like a DNS issue, check that the DNS returns an IP: 

```shell
# e.g. https://gitlab-review-331432-per-3db6b1.gitlab-review.app/ becomes
#      gitlab-review-331432-per-3db6b1.gitlab-review.app
dig +short <REVIEW_APP_DOMAIN_NAME>
```

You should get an IP back:

```shell
$ dig +short gitlab-review-331432-per-3db6b1.gitlab-review.app
34.170.218.245
```

If you don't, there might be a problem with the DNS. Consider reaching out to the `#g_engineering_productivity` channel for assistance, and to the `#infrastructure-lounge` channel in case there is some work done on the DNS (e.g. https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/18945#note_1423888701).

* [Optional] Are there DNS entries in AWS Route53?
  * Follow [the detailed instructions](#how-to-query-dns-records-managed-by-external-dns).

* Did we **upgrade the helm chart for review-apps recently**? Check [where we define `GITLAB_HELM_CHART_REF` env variable](https://gitlab.com/gitlab-org/gitlab/-/blob/0d0adafd7620ff5975a58b4a87a972b7211d442f/.gitlab/ci/review-apps/main.gitlab-ci.yml#L47), do a `git blame` to see when we last changed the chart.
  * If it was changed recently, consider reverting to a previous version.

* See whether we upgraded **cert-manager**, **external-dns** or **nginx-ingress** in [the engineering-productivity-infrastructure project](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/merge_requests?scope=all&state=merged)
  * If it was changed recently, consider reverting to a previous version.

* Check that the cluster and the nodes are healthy in https://console.cloud.google.com/kubernetes/list/overview?project=gitlab-review-apps.
  * Reach out to the `#g_engineering_productivity` channel if you see something unusual

* Check **the last time a cluster upgrade happened**:


```shell
gcloud container operations list
```

If it was around the time the review app deployments started failing, it might me the problem (and we cannot rollback to a previous GKE version, so we'll need to find the issue).

##### From inside the Kubernetes cluster

* Check the pods state in the review-apps namespace

```shell
review-apps
k get pods

# All should be running, and no recent restarts
```

* Check the logs of various pods (`cert-manager`, `external-dns`, `ingress-nginx`, `reflector`):

```shell
k logs -f <pod-name>
```

If you spot anything unusual in those pods, please reach out to the `#g_engineering_productivity` channel for further discussions.

* Check that **the TLS certificate is up-to-date**
  * * Follow [the detailed instructions](#tls-certificate-issues).

* What's next?
  * Check the various production incidents in this RUNBOOK
  * If you still haven't found the issue:
    * Make sure you [communicated in Slack that the review-apps are currently broken](#communication-to-the-company)
    * Consider [disabling review apps deployments](#temporarily-disable-review-apps)
    * Consider [reaching out for external help](##reaching-out-to-outside-the-team-for-help)

#### How to fix

N/A

### Reaching out to outside the team for help

If there is a pressing issue that cannot be resolved inside the Engineering Productivity team, please reach out to SREs:

* Write a message in Slack in the [#infrastructure-lounge](https://gitlab.slack.com/archives/CB3LSMEJV) channel
* If it needs urgent escalation, ping `@gitlab-com/gl-infa`

### Temporarily disable review-apps

When the `start-review-app-pipeline` child pipeline has an abnormally high failure rate, we do our best to investigate the root cause and resolve the problem **within 24 hours**.
If the 24 hours window has passed, we must disable review apps deployment: Please set [the `CI_REVIEW_APPS_ENABLED` CI/CD variable](https://gitlab.com/gitlab-org/gitlab/-/settings/ci_cd) to `false`.

The goal of the SLA is to encourage the best possible corrective action without losing the review apps feedback. If the problem becomes too complex, we should allow continuous investigation without disrupting engineering productivity.

### Review-apps are only accessible after ~20-30 minutes
#### The problem

(Actually happened: https://gitlab.com/gitlab-org/gitlab/-/issues/382134)

Some review apps, when accessed by their URLs, return a connection timed out for 10-60 minutes, then recover and are accessible again.

It's probably due to too many resources in the GKE cluster, which will make the GCP load balancers take a long time to be ready.

#### How to diagnose

* Go in [those GCP logs](https://console.cloud.google.com/logs/query;query=resource.type%3D%22k8s_cluster%22%0Aresource.labels.project_id%3D%22gitlab-review-apps%22%0A%22load%20balancer%22;timeRange=2022-11-17T14:21:45.449Z%2F2022-11-17T16:21:45.449975Z;cursorTimestamp=2022-11-17T14:53:30Z?project=gitlab-review-apps)
* Check the difference between `Ensuring load balancer` and `Ensured load balancer` for the same namespace (tip: filter by namespace as well by entering `"<your-namespace-name>"` into your query). An average time is between 30s-60s.

#### How to fix

* See how many resources are present on the cluster. You can have a look at [the latest 2-hourly scheduled pipeline in gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab/-/pipeline_schedules), and open the `review-k8s-resources-count-checks` job.
* Start deleting orphaned `services` and `deployments` (at least). There is a script in this issue: https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/issues/36#note_1174854342 to delete resources that do not belong to an active helm release. There is also a potentially simpler approach in this comment: https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/issues/39#note_1182500287. Please find out which option would work better for this situation, and take time to update this RUNBOOK entry afterwards!

### review-apps namespace: container restarts

#### The problem

The namespace `review-apps` runs the infrastructure we need for review-apps to function properly:

* cert-manager (to generate valid TLS certificates)
* external-dns (to dynamically create DNS entries for newly deployed review-apps)
* reflector (to copy TLS certificates across k8s namespaces)

If one of those pods is failing, it needs to be fixed soon, or the review-apps should be disabled until they are fixed.

#### How to diagnose

* Go see [the workloads of the review-apps kubernetes namespace](https://console.cloud.google.com/kubernetes/workload/overview?project=gitlab-review-apps&pageState=(%22savedViews%22:(%22i%22:%229c3d2a7bccdf4c49987f9de055ac88dd%22,%22c%22:%5B%22gke%2Fus-central1-b%2Freview-apps%22%5D,%22n%22:%5B%22review-apps%22%5D))) in the GCP console.
* Click on the pod that is failing
* Click on the `logs` tab to try to understand the issue

#### How to fix

1. Try to delete the pod:

```shell
# Switch to the review-apps GCP enviromnent / GKE cluster
review-apps

# Go inside the review-apps namespace
kubens review-apps

# See the pods deployed
kubectl get pods

kubectl delete pod <POD-NAME-TO-DELETE>

# Observe the pods
watch kubectl get pods

# Optional: Check the logs of the pod from the command line
kubectl logs -f <POD-NAME>
```

2. If that doesn't work, and if we upgraded the chart recently, revert the MR that introduced the new version if possible (check with the DRI for the MR)
3. If not, ask for guidance in Slack to the team (remember: this problem is pretty urgent and important to fix!)

### CPU usage for container is too high

#### The problem

We define resource requests and limits for containers for CPU and Memory, so that containers don't use too much CPU/Memory (more on requests and limits in https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#requests-and-limits).

When containers do use too much CPU/Memory, and we get an alert about it.

#### How to diagnose

We should receive an alert in `#review-apps-monitoring` slack channel from GCP monitoring.
#### How to fix

Increase the CPU request/limit for the container in question.

* If the container is in the `review-apps` k8s namespace, make an MR in the [engineering-productivity-infrastructure project](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure) ([see example](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/merge_requests/11)).
* If the container in a `review-*` k8s namespace (i.e. deployed review-apps), make an MR in the gitlab-org/gitlab project ([see example](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/101544)).

### Memory usage for container is too high

#### The problem

We define resource requests and limits for containers for CPU and Memory, so that containers don't use too much CPU/Memory (more on requests and limits in https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#requests-and-limits).

When containers do use too much CPU/Memory, and we get an alert about it.
#### How to diagnose

We should receive an alert in `#review-apps-monitoring` slack channel from GCP monitoring.

#### How to fix

Increase the memory request/limit for the container in question.

* If the container is in the `review-apps` k8s namespace, make an MR in the [engineering-productivity-infrastructure project](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure) ([see example](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/merge_requests/11)).
* If the container in a `review-*` k8s namespace (i.e. deployed review-apps), make an MR in the gitlab-org/gitlab project ([see example](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/101544)).

### Deployed review-apps: container restarts

#### The problem

A review app for the `gitlab-org/gitlab` project has been deployed, and has some containers restarting.

It's a low priority problem (much lower than a `review-apps namespace container restart` for instance), but it's still worth investigating, to understand whether we could improve the success rate of review-apps deployments.

#### How to diagnose

See [how to perform a root cause analysis](#how-to-perform-a-root-cause-analysis) for guidance.

#### How to fix

See [how to perform a root cause analysis](#how-to-perform-a-root-cause-analysis) for guidance.

### Database related errors in `review-deploy`, `review-qa-smoke`, or `review-qa-reliable`

#### The problem

Occasionally the state of a Review App's database could diverge from the database schema. This could be caused by changes to migration files or schema, such as a migration being renamed or deleted.

#### How to diagnose

This typically manifests in migration errors such as:

- migration job failing with a column that already exists
- migration job failing with a column that does not exist

#### How to fix

Please attempt to [redeploy Review App from a clean slate](https://docs.gitlab.com/ee/development/testing_guide/review_apps.html#redeploy-review-app-from-a-clean-slate)

### Release failed with `ImagePullBackOff`

#### The problem

One of the container images used to deploy the review app is not found by Kubernetes when trying to launch a container.

#### How to diagnose

To check that the Docker images were created, run the following Docker command:

```shell
`DOCKER_CLI_EXPERIMENTAL=enabled docker manifest repository:tag`
```

The output of this command indicates if the Docker image exists. For example:

```shell
DOCKER_CLI_EXPERIMENTAL=enabled docker manifest inspect registry.gitlab.com/gitlab-org/build/cng-mirror/gitlab-rails-ee:39467-allow-a-release-s-associated-milestones-to-be-edited-thro
```

#### How to fix

If the Docker image does not exist:

- Verify the `image.repository` and `image.tag` options in the `helm upgrade --install` command match the repository names used by CNG-mirror pipeline.
- Look further in the corresponding downstream CNG-mirror pipeline in `review-build-cng` job.

### Node count is always increasing (never stabilizing or decreasing)

#### The problem

It could be a sign that the `review-cleanup` job is failing to cleanup stale Review Apps and Kubernetes resources.

#### How to diagnose

Look at the latest `review-cleanup` job log, and look for any
unexpected failure.

#### How to fix

Try to fix the errors you identified (and please update this section with the errors you found, and how you fixed them).

### p99 CPU utilization is at 100% for most of the nodes and/or many components

#### The problem

This could be a sign that Helm is failing to deploy Review Apps. When Helm has a
lot of `FAILED` releases, it seems that the CPU utilization is increasing, probably due to Helm or Kubernetes trying to recreate the components.

#### How to diagnose

Look at a recent `review-deploy` job log.

**Useful commands:**

```shell
# Identify if node spikes are common or load on specific nodes which may get rebalanced by the Kubernetes scheduler
kubectl top nodes | sort --key 3 --numeric

# Identify pods under heavy CPU load
kubectl top pods | sort --key 2 --numeric
```

#### How to fix

Try to fix the errors you identified (and please update this section with the errors you found, and how you fixed them).

### The `logging/user/events/FailedMount` chart is going up

#### The problem

This could be a sign that there are too many stale secrets and/or configuration maps.

#### How to diagnose

Look at [the list of Configurations](https://console.cloud.google.com/kubernetes/config?project=gitlab-review-apps)
or `kubectl get secret,cm --sort-by='{.metadata.creationTimestamp}' | grep 'review-'`.

Any secrets or configuration maps older than 5 days are suspect and should be deleted.

**Useful commands:**

```shell
# List secrets and config maps ordered by created date
kubectl get secret,cm --sort-by='{.metadata.creationTimestamp}' | grep 'review-'
```

#### How to fix

Any secrets or configuration maps **older than 5 days are suspect and should be deleted**:

```shell
# Delete all secrets that are 5 to 9 days old
kubectl get secret --sort-by='{.metadata.creationTimestamp}' | grep '^review-' | grep '[5-9]d$' | cut -d' ' -f1 | xargs kubectl delete secret

# Delete all secrets that are 10 to 99 days old
kubectl get secret --sort-by='{.metadata.creationTimestamp}' | grep '^review-' | grep '[1-9][0-9]d$' | cut -d' ' -f1 | xargs kubectl delete secret

# Delete all config maps that are 5 to 9 days old
kubectl get cm --sort-by='{.metadata.creationTimestamp}' | grep 'review-' | grep -v 'dns-gitlab-review-app' | grep '[5-9]d$' | cut -d' ' -f1 | xargs kubectl delete cm

# Delete all config maps that are 10 to 99 days old
kubectl get cm --sort-by='{.metadata.creationTimestamp}' | grep 'review-' | grep -v 'dns-gitlab-review-app' | grep '[1-9][0-9]d$' | cut -d' ' -f1 | xargs kubectl delete cm
```

### external-dns: errors in logs

#### The problem

external-dns pod is having errors error in its container logs.

#### How to diagnose

We should have received an alert in Slack about this (we have an alert configured in GCP monitoring to alert of external-dns errors).

To check the logs manually,

* `kubens review-apps`
* `kubectl get pods`
* `kubectl logs -f <external-dns-pod-name>`
* Search for errors
#### How to fix

If you see an error similar to `failed to sync *v1.Pod: context deadline exceeded`, have a look at [this RUNBOOK entry](#external-dns-failed-to-sync-v1pod-context-deadline-exceeded).

Otherwise, you'll have to find out the root cause (the team is here to help as well!). Please also create an issue to update this RUNBOOK entry with your new findings.

### external-dns: failed to sync *v1.Pod: context deadline exceeded
#### The problem

external-dns pod is failing with the following error in the logs:

```shell
level=fatal msg="failed to sync *v1.Pod: context deadline exceeded"
```

Most probably, the GKE cluster has too many pods/resources, and external-dns has a 30s timeout for this sync operation.

This problem happened to us in https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/issues/35, and we created https://github.com/kubernetes-sigs/external-dns/issues/3173 for external-dns to report the problem.

#### How to diagnose

* `kubens review-apps`
* `kubectl get pods`
* `kubectl logs -f <external-dns-pod-name>`
* You should see `failed to sync *v1.Pod: context deadline exceeded`
#### How to fix

Follow the same resolution steps as in [Review-apps are only accessible after ~20-30 minutes](#review-apps-are-only-accessible-after-20-30-minutes)

### external-dns: Troubleshoot a pending `dns-gitlab-review-app-external-dns` Deployment

#### The problem

[In the past](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/62834), it happened that the `dns-gitlab-review-app-external-dns` Deployment was in a pending state, effectively preventing all the Review Apps from getting a DNS record assigned, making them unreachable via domain name.

This in turn prevented other components of the Review App to properly start
(for example, `gitlab-runner`).

After some digging, we found that new mounts fail when performed
with transient scopes (for example, pods) of `systemd-mount`:

```plaintext
MountVolume.SetUp failed for volume "dns-gitlab-review-app-external-dns-token-sj5jm" : mount failed: exit status 1
Mounting command: systemd-run
Mounting arguments: --description=Kubernetes transient mount for /var/lib/kubelet/pods/06add1c3-87b4-11e9-80a9-42010a800107/volumes/kubernetes.io~secret/dns-gitlab-review-app-external-dns-token-sj5jm --scope -- mount -t tmpfs tmpfs /var/lib/kubelet/pods/06add1c3-87b4-11e9-80a9-42010a800107/volumes/kubernetes.io~secret/dns-gitlab-review-app-external-dns-token-sj5jm
Output: Failed to start transient scope unit: Connection timed out
```

This probably happened because the GitLab chart creates 67 resources, leading to
a lot of mount points being created on the underlying GCP node.

The [underlying issue seems to be a `systemd` bug](https://github.com/kubernetes/kubernetes/issues/57345#issuecomment-359068048)
that was fixed in `systemd` `v237`. Unfortunately, our GCP nodes are currently
using `v232`.

#### How to diagnose

For the record, the debugging steps to find out this issue were:

1. Switch kubectl context to `review-apps-ce` (we recommend using [`kubectx`](https://github.com/ahmetb/kubectx/))
1. `kubectl get pods | grep dns`
1. `kubectl describe pod <pod name>` & confirm exact error message
1. Web search for exact error message, following rabbit hole to [a relevant Kubernetes bug report](https://github.com/kubernetes/kubernetes/issues/57345)
1. Access the node over SSH via the GCP console (**Computer Engine > VM
   instances** then click the "SSH" button for the node where the `dns-gitlab-review-app-external-dns` pod runs)
1. In the node: `systemctl --version` => `systemd 232`
1. Gather some more information:
   - `mount | grep kube | wc -l` (returns a count, for example, 290)
   - `systemctl list-units --all | grep -i var-lib-kube | wc -l` (returns a count, for example, 142)
1. Check how many pods are in a bad state:
   - Get all pods running a given node: `kubectl get pods --field-selector=spec.nodeName=NODE_NAME`
   - Get all the `Running` pods on a given node: `kubectl get pods --field-selector=spec.nodeName=NODE_NAME | grep Running`
   - Get all the pods in a bad state on a given node: `kubectl get pods --field-selector=spec.nodeName=NODE_NAME | grep -v 'Running' | grep -v 'Completed'`

#### How to fix

To resolve the problem, we needed to (forcibly) drain some nodes:

1. Try a normal drain on the node where the `dns-gitlab-review-app-external-dns`
   pod runs so that Kubernetes automatically move it to another node: `kubectl drain NODE_NAME`
1. If that doesn't work, you can also perform a forcible "drain" the node by removing all pods: `kubectl delete pods --field-selector=spec.nodeName=NODE_NAME`
1. In the node:
   - Perform `systemctl daemon-reload` to remove the dead/inactive units
   - If that doesn't solve the problem, perform a hard reboot: `sudo systemctl reboot`
1. Uncordon any cordoned nodes: `kubectl uncordon NODE_NAME`

In parallel, since most Review Apps were in a broken state, we deleted them to
clean up the list of non-`Running` pods.
Following is a command to delete Review Apps based on their last deployment date
(current date was June 6th at the time) with

```shell
helm ls -d | grep "Jun  4" | cut -f1 | xargs helm delete --purge
```

**Mitigation steps taken to avoid this problem in the future**

We've created a new node pool with smaller machines to reduce the risk
that a machine reaches the "too many mount points" problem in the future.

### TLS certificate issues

#### The problem

Either we have received an email from Let's Encrypt telling us that the TLS certificate for the review-apps is about to expire, or we notice that review-apps don't have a valid TLS certificate to use.

#### How to diagnose

Go to a review app deployment:

* [Pick a pipeline that deployed review apps](https://gitlab.com/gitlab-org/gitlab/-/pipelines?page=1&scope=all&status=success)
* Go to the `review-deploy` job
* Search for `.gitlab-review.app`, and go to that URL
* See if your browser warns you about anything

#### How to fix

1. Check that the secret `review-apps-tls` is present in the namespace. It should have been copied from the `review-apps` k8s namespace by reflector.

```shell
kubens <namespace-to-investigate>
k get secrets review-apps-tls
```

If it is absent, you can cleanup the review app, and redeploy it: https://docs.gitlab.com/ee/development/testing_guide/review_apps.html#redeploy-review-app-from-a-clean-slate.

1. Have a look at the logs of the cert-manager pods in the `review-apps` namespace, and try to understand the problem:

```shell
review-apps
kubens review-apps
k get po
k logs -f <pod-name>
```

2. You can also have a look at the `certificate` resource to see the status of the certificate from cert-manager's perspective:

```shell
review-apps
kubens review-apps
k describe certificates
```

You should see `Certificate is up to date and has not expired` under `Status`, and the following three entries are important to check:

```shell
  Not After:               2023-07-11T07:39:46Z
  Not Before:              2023-04-12T07:39:47Z
  Renewal Time:            2023-06-11T07:39:46Z
```

3. Also, have a look at [the learning section on cert-manager](#cert-manager) for more insights.

### Namespace stuck in `Terminating` state

#### The problem

* We received an alert in Slack about a namespace stuck in `Terminating` state
* `review-delete-deployment` or other review-apps CI jobs are timing out after 90min (e.g. https://gitlab.com/gitlab-org/gitlab/-/jobs/4555317683) when trying to deploy or delete a review app.

#### How to diagnose

```shell
k get ns -A | grep -v Active
```

#### How to fix

Follow [this great article from GCP](https://cloud.google.com/kubernetes-engine/docs/troubleshooting#namespace_stuck_in_terminating_state) on how to troubleshoot this issue.

### 2 default StorageClasses were found

#### The problem

See https://gitlab.com/gitlab-org/gitlab/-/issues/416849.

The following message is shown when deploying review-apps:

```shell
Error: persistentvolumeclaims "review-mastermain-mk1cr0-minio" is forbidden: Internal error occurred: 2 default StorageClasses were found
```

#### How to diagnose

```shell
k get storageclass
```

There should **always be only one default storageclass**.

#### How to fix

Patch one of the storage class (keep the oldest storage class as the default) to not be the default anymore:

```shell
k patch storageclass standard-rwo -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'
```

## Maintenance tasks

### review-cleanup job failed

#### Context

This job runs two scripts to clean up review-apps resources:

1. [scripts/review_apps/automated_cleanup.rb](https://gitlab.com/gitlab-org/gitlab/blob/master/scripts/review_apps/automated_cleanup.rb)
1. [scripts/review_apps/gcp_cleanup.sh](https://gitlab.com/gitlab-org/gitlab/blob/master/scripts/review_apps/gcp_cleanup.sh)

This is a high-priority problem, as the last major review app incident was due to too many resources in the cluster (see https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/116#note_1179429380 for context).
#### Instructions

* Search for an error in the failed job output (literally: `CMD+F` and search for the word `error`)
* Diagnose the problem
* Make an MR to fix if you found the root cause, or create an issue in https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/issues/new and mention `@gl-dx/eng-prod` for awareness.

### review-gcp-quotas-checks job failed

This job runs one script to check GCP quotas:

1. [scripts/review_apps/gcp-quotas-checks.rb](https://gitlab.com/gitlab-org/gitlab/blob/master/scripts/review_apps/gcp-quotas-checks.rb)

This is a high-priority problem, as the last major review app incident was due to too many resources in the cluster (see https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/116#note_1179429380 for context).
#### Instructions

* Search for an error in the failed job output (literally: `CMD+F` and search for the word `error`)
* Diagnose the problem
* Try to remove k8s resources that could diminish the quota.
* If the problem is urgent, increase the problematic quotas at https://console.cloud.google.com/iam-admin/quotas by following https://support.google.com/cloud/answer/6075746?hl=en

### review-k8s-resources-count-checks job failed

This job runs one script to check Kubernetes resources count:

1. [scripts/review_apps/k8s-resources-count-checks.sh](https://gitlab.com/gitlab-org/gitlab/blob/master/scripts/review_apps/k8s-resources-count-checks.sh)

This is a high-priority problem, as the last major review app incident was due to too many resources in the cluster (see https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/116#note_1179429380 for context).
#### Instructions

* See which resources are too high
* Try to understand why they are so high
  * Check the number of review apps we have deployed with `helm ls -A | wc -l | xargs`
  * Pick a random namespace (`kubectl get ns`, then `kubens <review-app-namespace>`), and check how many of those resources it has on average.
  * Compute the expected number of resources by multiplying the number of review-apps deployed and the average number of resources per namespace.
* Make an MR to fix if you found the root cause, or create an issue in https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/issues/new and mention `@gl-dx/eng-prod` for awareness.

### Upgrade GitLab helm chart

#### Context

To deploy an application on Kubernetes, you use a helm chart (e.g. [PostgreSQL](https://artifacthub.io/packages/helm/bitnami/postgresql)).

To deploy GitLab, we also use a helm chart: https://gitlab.com/gitlab-org/charts/gitlab.

This helm chart is regularly upgraded, and we should keep it up to date to test the latest GitLab version in review-apps.
#### Instructions

* Locate the `GITLAB_HELM_CHART_REF` env variable in gitlab-org/gitlab project (`.gitlab/ci/review-apps/main.gitlab-ci.yml`)
* Change its value to the commit sha from the version you'd like: https://gitlab.com/gitlab-org/charts/gitlab/-/commits/master?search=Update+version+mapping+for
* Check the [GitLab chart changelog](https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/CHANGELOG.md) for the version you are upgrading to, and **check every MR** to check whether we need to change anything.
* Create an MR and check that the review-app was successfully deployed.

See an example MR: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/103618/diffs

Once it's merged:

* Have a look at the new few entries in [#review-apps-broken on Slack](https://gitlab.slack.com/archives/C011EM0RB71) to see any potential problem.
* If everything looks fine for the last 8-12 hours, the upgrade was successful!

### Check the GCP console cluster page

#### Context

We noticed that the GKE alerts we receive for the review-apps cluster cannot be fetched from anywhere else than the GCP console (see https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/issues/34#note_1184949473).

To ensure that the GKE cluster remains healthy, we should check periodically those messages, and create issues appropriately.

#### Instructions

* Go to the GCP console page for the GKE review-apps cluster: https://console.cloud.google.com/kubernetes/clusters/details/us-central1-b/review-apps/details?project=gitlab-review-apps
* Look at the banners at the top. Below is an example at the time of writing:
  * ![review-apps-cluster-notifications.png](../assets/review-apps-cluster-notifications.png)
* Make sure we have an issue open for it in https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/issues/
* If we don't yet have an issue for it, please create one and mention `@gl-dx/eng-prod` for awareness
* If we are stuck with an urgent problem we cannot resolve within the team, please [reach out for external help](##reaching-out-to-outside-the-team-for-help).

### Manually upgrade the GKE Cluster

#### Context

The cluster is upgraded manually. When a new version will be released/deployed, we receive a Slack notification in the `#review-apps-monitoring` channel. Below are the things we need to do before/after a Kubernetes cluster upgrade.

#### Instructions

* Check [the distribution EPICs](https://gitlab.com/groups/gitlab-org/-/epics/4827) to see whether we can upgrade to the next minor version (the GitLab helm chart needs to be compatible with the new Kubernetes version)
* Read the changelog/deprecations for the upcoming version in detail (in particular the deprecations/removals)
  * Create follow-up issues for the things that we'll need to change (consider adding a due date to the issue)
  * Alternative: Use [the `kubent` tool](https://github.com/doitintl/kube-no-trouble) to see whether we have deprecated APIs to change
* - [Upgrade kubectl in EP container images](#upgrade-kubectl-in-ep-container-images)
* Consider the charts present in [the engineering-productivity-infrastructure repo](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure), and think whether they would need to be upgraded before or after the k8s cluster upgrade.
* Do the manual upgrades as documented in GKE
  * [Manually upgrading the control plane](https://cloud.google.com/kubernetes-engine/docs/how-to/upgrading-a-cluster?_ga=2.129574860.-1547203733.1644829425#upgrade_cp)
  * [Manually upgrading a node pool](https://cloud.google.com/kubernetes-engine/docs/how-to/upgrading-a-cluster?_ga=2.129574860.-1547203733.1644829425#upgrade_nodes)
* [Check the GCP console for the GKE cluster after the upgrade](#check-the-gcp-console-cluster-page), and look for potential problems.
  * If we are stuck with an urgent problem we cannot resolve within the team, please [reach out for external help](##reaching-out-to-outside-the-team-for-help).

### Upgrade kubectl in EP container images

#### Context

`kubectl` need to [stay within one minor version from the Kubernetes cluster version](https://kubernetes.io/releases/version-skew-policy/#kubectl).

Since we auto-upgrade some of our GKE clusters, we need to periodically check to upgrade `kubectl`.

#### Instructions

* Upgrade the `kubectl` utility in the EP container image ([example MR](https://gitlab.com/gitlab-org/gitlab-build-images/-/merge_requests/834)).
* Upgrade triage-ops `kubectl` image ([example MR](https://gitlab.com/gitlab-org/quality/triage-ops/-/merge_requests/1706)).
* Upgrade [review-apps `kubectl` image in `REVIEW_APPS_IMAGE`](https://gitlab.com/gitlab-org/gitlab/-/blob/359330267b91afb93c30073b74047dbfc05ba1ab/.gitlab-ci.yml#L228).

### Check the GKE cluster resources requests

#### Context

For an introduction to Kubernetes requests and limits, have a look at https://cloud.google.com/blog/products/containers-kubernetes/kubernetes-best-practices-resource-requests-and-limits.

In https://gitlab.com/gitlab-org/gitlab/-/blob/98d888c565031f045a456d58153ac192cc8df78c/scripts/review_apps/base-config.yaml, we set resources requests/limits for various review-apps containers ([see an example for gitaly containers](https://gitlab.com/gitlab-org/gitlab/-/blob/98d888c565031f045a456d58153ac192cc8df78c/scripts/review_apps/base-config.yaml#L24-37)).

GKE is only using **requests** to schedule pods onto nodes. What this means is that **the number of GKE nodes is ultimately defined by the requests we set on the review-apps containers**. This will ultimately influence the price we pay for the cluster.

We need to ensure that those requests are enough for the review-apps to function properly, but not too high so that we don't spend money on unused resources.

The goal of this task is to ensure that the requests we have set are "just right".

#### Instructions

* Go to the GCP console page for the GKE review-apps cluster costs: https://console.cloud.google.com/kubernetes/list/cost?project=gitlab-review-apps
* Zoom out to the last month or the last 3 months
* Check the `CPU utilization (CPU)` and `Memory Utilization (GB)`, and see whether our resource usage is close to the requests we have set: the closer they are from one another, the better.
* If the resource usage and the resource requests are too far from one another, investigate where we are over-committing or undercommitting our requests:
  * Use the **GCP metrics explorer** to find out ([see an example for gitlab-shell container](https://gitlab.com/gitlab-org/gitlab/-/blob/98d888c565031f045a456d58153ac192cc8df78c/scripts/review_apps/base-config.yaml#L52))
* Make a merge request to decrease CPU/memory requests where applicable ([see an example](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/118008))

## Learning

### Triaging incidents

- Set the :ack: emoji on the slack notification, to let other people know that the incident is being looked at.
- Since we do a few retries of the review-deploy job in the CI, select the first failed review-deploy job for investigation (this is until https://gitlab.com/gitlab-org/gitlab/-/issues/404583 is worked on)
- Have a look at the [open and closed incidents](https://gitlab.com/gitlab-org/quality/engineering-productivity/review-apps-broken-incidents/-/issues/?sort=created_date&state=opened&first_page_size=20), and try to find the root cause incident related to the incident you're looking at.
- If you are unsure about the root cause of the incident, please have a look at [how to perform a root cause analysis](#how-to-perform-a-root-cause-analysis)
- If there's no known incidents, please [create an issue](https://gitlab.com/gitlab-org/quality/engineering-productivity/review-apps-broken-incidents/-/issues/new) with [the `Broken Review App RCA` template](https://gitlab.com/gitlab-org/quality/engineering-productivity/review-apps-broken-incidents/-/blob/main/.gitlab/issue_templates/Broken%20Review%20App%20RCA.md).

- Set the incident as a duplicate of the existing root cause incident:

```shell
/assign me
/copy_metadata #<root-cause-incident-id>
/duplicate #<root-cause-incident-id>
```

### General Kubernetes learning resources

- [Understanding Kubernetes Terminology](https://about.gitlab.com/blog/2020/07/30/kubernetes-terminology/)
- [Kubernetes Tutorial for Beginners](https://www.youtube.com/watch?v=X48VuDVv0do) - video about 4 hours
- [Kubernetes in 5 minutes](https://www.youtube.com/watch?v=PH-2FfFD2PU)
- [What is helm](https://www.youtube.com/watch?v=-ykwb1d0DXU)

### Networking: From deploying a review app to having it accessible to the outside world

When we deploy a review app, we are deploying [the GitLab CNG chart](https://gitlab.com/gitlab-org/charts/gitlab/-/tree/master/charts/gitlab) to the review-apps Kubernetes cluster ([see the review-deploy CI/CD job](https://gitlab.com/gitlab-org/gitlab/blob/master/.gitlab/ci/review-apps/main.gitlab-ci.yml#L99-132)).

When we deploy this chart, a lot of steps need to happen in order to have an URL to the review app (e.g. https://gitlab-review-nh-test-fu-4s1isx.gitlab-review.app/).

#### From nothing to a k8s service, to a GCP load balancer

![ingress-controller diagram](https://storage.googleapis.com/gcp-community/tutorials/nginx-ingress-gke/Nginx%20Ingress%20on%20GCP%20-%20Fig%2001.png)

(image fetched from https://cloud.google.com/community/tutorials/nginx-ingress-gke)

The key ingredient in the networking setup is the [nginx-ingress helm chart](https://kubernetes.github.io/ingress-nginx/).

The main component is called the `nginx-ingress controller`, or just ingress controller. Think of this component as a big load balancer inside a Kubernetes cluster, that will create routes to pods based on the rules present in [Kubernetes ingress resources](https://kubernetes.io/docs/concepts/services-networking/ingress/). It monitors ingresses, and will create/remove load balancers based on what changed there.

When deploying the chart, it creates a [Kubernetes service](https://kubernetes.io/docs/concepts/services-networking/service/) of [type LoadBalancer](https://kubernetes.io/docs/concepts/services-networking/service/#loadbalancer) for the nginx-ingress controller, which will in turn create an [External TCP/UDP Network Load Balancer in GCP](https://cloud.google.com/kubernetes-engine/docs/how-to/exposing-apps#creating_a_service_of_type_loadbalancer).

Already a few things to note:

1. There is **one public IP to access one review app**, and this public IP will point to the **Kubernetes service of the nginx-ingress controller**.
1. This setup takes some time (probably ~1-2 minutes).
1. Those GCP load balancers are **passthrough**, and **the TCP connections are terminated by the backend**. In our case, the backend are **the ingress-controller pods**.

#### From an IP to a DNS name

We need to create a DNS entry that will map a hostname to the IP of the GCP load balancer we created earlier. This is done thanks to [`external-dns`](https://github.com/kubernetes-sigs/external-dns), a chart we deploy that will magically create DNS entries in an Amazon managed DNS.

#### From HTTP to HTTPS

We now have a publicly accessible load balancer for our review-app, and an ingress controller that will create "routes" to our pods, but we don't have HTTPS.

For any review app, we do a few things here:

* We have **one TLS wildcard certificate for all of the review apps** that we copy to the newly deployed review-apps namespace, thanks to `Kubernetes Reflector`, a.k.a. `reflector` ([see this section to learn more](#reflector))
* The `TLS certificate` is **stored in a secret**, which is referenced by every Kubernetes ingress resource that needs to be accessible via HTTPS (so all of them)
* This TLS wildcard certificate **stays in the Kubernetes cluster** (except when serving the HTTPS page of course): the ingress-controller will use that secret to configure the "internal load balancers". It's visible at the very beginning of the pod logs of the ingress-controller:

```shell
 $ k logs -f review-nh-test-fu-4s1isx-nginx-ingress-controller-64456dd72njdp
-------------------------------------------------------------------------------
NGINX Ingress controller
  Release:       v1.0.4
  Build:         9b78b6c197b48116243922170875af4aa752ee59
  Repository:    https://github.com/kubernetes/ingress-nginx
  nginx version: nginx/1.19.9

-------------------------------------------------------------------------------

W1110 01:42:49.883379       7 client_config.go:615] Neither --kubeconfig nor --master was specified.  Using the inClusterConfig.  This might not work.
I1110 01:42:49.883553       7 main.go:221] "Creating API client" host="https://10.0.0.1:443"
I1110 01:42:49.891456       7 main.go:265] "Running in Kubernetes cluster" major="1" minor="23" git="v1.23.8-gke.1900" state="clean" commit="79209257257c051b27df67c567755783eda93353" platform="linux/amd64"
I1110 01:42:49.896112       7 main.go:86] "Valid default backend" service="review-nh-test-fu-4s1isx/review-nh-test-fu-4s1isx-nginx-ingress-defaultbackend"

# First important line
I1110 01:42:50.054465       7 main.go:104] "SSL fake certificate created" file="/etc/ingress-controller/ssl/default-fake-certificate.pem"

I1110 01:42:50.962913       7 nginx.go:253] "Starting NGINX Ingress controller"
I1110 01:42:50.974021       7 event.go:282] Event(v1.ObjectReference{Kind:"ConfigMap", Namespace:"review-nh-test-fu-4s1isx", Name:"review-nh-test-fu-4s1isx-nginx-ingress-tcp", UID:"dcc233c0-4232-4154-b48e-b96f340bd601", APIVersion:"v1", ResourceVersion:"2935228260", FieldPath:""}): type: 'Normal' reason: 'CREATE' ConfigMap review-nh-test-fu-4s1isx/review-nh-test-fu-4s1isx-nginx-ingress-tcp
I1110 01:42:50.974138       7 event.go:282] Event(v1.ObjectReference{Kind:"ConfigMap", Namespace:"review-nh-test-fu-4s1isx", Name:"review-nh-test-fu-4s1isx-nginx-ingress-controller", UID:"b2683980-b0c3-48d6-8a5b-eaf46039c8d8", APIVersion:"v1", ResourceVersion:"2935228269", FieldPath:""}): type: 'Normal' reason: 'CREATE' ConfigMap review-nh-test-fu-4s1isx/review-nh-test-fu-4s1isx-nginx-ingress-controller
I1110 01:42:52.872613       7 store.go:371] "Found valid IngressClass" ingress="review-nh-test-fu-4s1isx/review-nh-test-fu-4s1isx-kas" ingressclass="review-nh-test-fu-4s1isx-nginx"
I1110 01:42:52.872794       7 event.go:282] Event(v1.ObjectReference{Kind:"Ingress", Namespace:"review-nh-test-fu-4s1isx", Name:"review-nh-test-fu-4s1isx-kas", UID:"607bd584-96ab-45bd-afd8-89719e2cd399", APIVersion:"networking.k8s.io/v1", ResourceVersion:"2935228517", FieldPath:""}): type: 'Normal' reason: 'Sync' Scheduled for sync

# Second important line
I1110 01:42:52.873302       7 backend_ssl.go:66] "Adding secret to local store" name="review-nh-test-fu-4s1isx/review-apps-tls"
```

In the "first important line" above, you can see that the ingress controller creates a `SSL fake certificate`, which is a self-signed certificate used for the default backend (i.e. all of the HTTP routes that don't have an ingress that covers them).

In the "second important line", you can see that the secret containing the TLS wildcard certificate is loaded by the ingress controller.

You can also verify this in your browser:

* Go to any deployed review-app from your terminal with `kubens`: `kubens <review-app-namespace>`
* Get the load balancer IP from the ingress-controller service: `kubectl get services`
* Access it in your browser, and display the certificate: https://35.223.172.37/
  * The certificate will be from `Kubernetes Ingress Controller Fake Certificate`. this is because we haven't defined a route for this particular route (we only defined it for the actual host).
* Now, access the hostname instead, and check the certificate (in my example, https://gitlab-review-nh-test-fu-4s1isx.gitlab-review.app/)
  * The certificate will be from `Let's Encrypt`. This is because there is a defined route for this URL, and there is a local load balancer setup with the correct TLS certificate.

Note that both certificates were served by the ingress-controller! No certificates was served by the GCP load balancer (this is quite important).

### Tips for working with GCP logs

In this section, we share some tips on how to search for logs in an efficient way.

#### Tip: The short URL

https://console.cloud.google.com/logs

#### Tip: Filter your query using the sidebar on the left

Those are excellent filters to start with (in particular if you're looking at cluster events):

![](../assets/gcp-logs-resource-type-sidebar.png)

Once you clicked a resource type, you can also add additional filters, such as `cluster name`, or a particular log.

Whenener you click on a filter, it will add a line in your query such as `resource.labels.cluster_name="review-apps"`.

#### Tip: Use the "hide similar entries" feature!

This will make the logs much more useful. Just like the left sidebar, it will also add an entry into your search query: 

```shell
--Hide similar entries
-(protoPayload.methodName="io.k8s.core.v1.configmaps.update")
--End of hide similar entries
```

![](../assets/gcp-logs-hide-similar-entries.png)

#### Tip: When unsure, use "global search"

Global search: entering a string in double quotes in the query (e.g. `"load balancer"` or `"failure"`).

Global search in GCP logs is slow, but it's really powerful. See below for practical use-cases for it.
##### Tip: review-apps - Search for the namespace name

Add the namespace name in double quotes in your search! This will give you all of the logs entries in GCP that concern the Kubernetes namespace (e.g. container logs, GCP load balancer informations, ...):

```shell
resource.type="k8s_cluster"
resource.labels.cluster_name="review-apps"
"review-103068-use-q900wt"
```

##### Tip: review-apps - search for "load balancer"

A specific trick: searching for `"load balancer"` will allow you to see when the GCP load balancer was setup for which review-app!

If you also filter by the review-app, you'll be able to see how long it took to actually setup the GCP load balancer:

![](../assets/gcp-logs-load-balancer-search.png)

### `review-deploy`

#### How to perform a root cause analysis

- Look at the logs of the failing CI/CD job, and try to identify the root cause
  - Most of the problems will happen around the helm deployment. Look around the `Deploying with helm command` line for potential errors.
  - We display debugging data in case a deployment failed: those lines are prefixed with `[debugging data]`.
- If you cannot determine the root cause of the issue, you might want to look at the application logs for the deployed review app (described in `Looking at container logs in GCP` below).

**Known Red Herrings**

* `ERROR:  relation "postgres_partitioned_tables" does not exist at character 85` generally happens at the very beginning of the postgreSQL container. Those errors likely happen because the application container tries to access the database when it hasn't been seeded yet by the `migrations` container. Those errors are generally not the root cause problem.

* `Error: UPGRADE FAILED: another operation (install/upgrade/rollback) is in progress` is related to the job retry we are doing, that are likely overstepping. https://gitlab.com/gitlab-org/gitlab/-/issues/404583 should remove this red herring altogether.

**Looking at container logs in GCP**

* Search for `namespace` in the CI/CD job, and copy the namespace name (i.e. starts with `review-`)
  * If you see `Error: timed out waiting for the condition`, it means that some pod failed, and Helm could not mark the deployment as successful. You can see which pod failed right below that error message. To find the root cause, you can follow the rest of the instructions in this section.
* Check the pods that failed in the [GKE workloads UI](https://console.cloud.google.com/kubernetes/workload/overview?project=gitlab-review-apps&pageState=(%22savedViews%22:(%22i%22:%229c3d2a7bccdf4c49987f9de055ac88dd%22,%22c%22:%5B%22gke%2Fus-central1-b%2Freview-apps%22%5D,%22n%22:%5B%5D),%22workload_list_table%22:(%22s%22:%5B(%22i%22:%22status%2Fstatus_sort_key%22,%22s%22:%220%22),(%22i%22:%22metadata%2Fname%22,%22s%22:%220%22)%5D)))
  * select the namespace you'd like to investigate
  * Click on a pod that failed
  * Check its logs on the logs tab
* If don't anything special in the pods, try to find out whether a node was in repair mode with `gcloud container operations list`
* If you don't see any GKE upgrades or nodes in repair mode, try to see whether a node restarted at the time of the event with [this GCP logs query](https://console.cloud.google.com/logs/query;query=resource.type%3D%22k8s_node%22%20AND%20%0Aresource.labels.cluster_name%3D%22review-apps%22%20AND%0Alog_id%2528%22events%22%2529%20AND%20%0AjsonPayload.reason%3D%22Rebooted%22;timeRange=2022-11-07T19:17:00.000Z%2F2022-11-08T02:53:00.000Z;cursorTimestamp=2022-11-08T01:47:34Z?project=gitlab-review-apps).

Hopefully, you should have found out the problem. If not, continue to diagnose, and [write down additional debugging steps](#unknown-problems).

#### Reproduce `review-deploy` job locally

Since the `review-deploy` job is a crucial part of the review apps process, it can be scary to change. This document is here to help you validate that your changes in this part of the codebase are what you expect them to be.

**Benefits**

* Increase reproducibility in our processes (for faster, more accessible debugging)
* Shift Left: Validate changes you've made in this section before pushing your code
* Faster feedback loop: You don't have to wait for the CI/CD to complete

#### Generate the helm command used to deploy the review application

```shell
docker run -it -v $PWD:/app ruby:2.7-alpine3.13 ash
apk add bash && bash

# Comment out the final deploy command for the `deploy` function.
#
# Rationale: We just want to see the command that we'll use to deploy the review app to a cluster.
sed -i 's/^  eval/#/' /app/scripts/review_apps/review-apps.sh

# These are dummy values - feel free to change them
export CI_COMMIT_REF_NAME=local-branch
export CI_COMMIT_REF_SLUG=local-branch
export CI_COMMIT_SHORT_SHA=xxx
export CI_DEFAULT_BRANCH=master
export CI_ENVIRONMENT_SLUG=review-local
export CI_JOB_ID=8291
export CI_JOB_URL=https://gitlab.com/gitlab-org/gitlab/-/jobs/1234
export CI_MERGE_REQUEST_IID=9876
export CI_PIPELINE_ID=7777
export CI_PIPELINE_URL=https://gitlab.com/gitlab-org/gitlab/-/pipelines/1234
export CI_PROJECT_ID=1234
export CI_PROJECT_PATH=gitlab/gitlab
export gitaly_image_tag=local-branch
export GITALY_VERSION=1.2.3
export gitlab_gitaly_image_repository=registry.gitlab.com/gitlab-org/build/cng-mirror/gitaly
export GITLAB_HELM_CHART_REF=CHANGEME
export gitlab_shell_image_repository=registry.gitlab.com/gitlab-org/build/cng-mirror/gitlab-shell
export GITLAB_SHELL_VERSION=local-branch
export gitlab_sidekiq_image_repository=registry.gitlab.com/gitlab-org/build/cng-mirror/gitlab-sidekiq-ee
export gitlab_toolbox_image_repository=registry.gitlab.com/gitlab-org/build/cng-mirror/gitlab-toolbox-ee
export gitlab_webservice_image_repository=registry.gitlab.com/gitlab-org/build/cng-mirror/gitlab-webservice-ee
export gitlab_workhorse_image_repository=registry.gitlab.com/gitlab-org/build/cng-mirror/gitlab-workhorse-ee
export HELM_INSTALL_TIMEOUT=20m
export HOST_SUFFIX=review-local
export namespace=review-local
export release=review-local
export REVIEW_APPS_DOMAIN=gitlab-review.app
export REVIEW_APPS_SENTRY_DSN=xxx
export sentry_enabled=true

# Copy the entire block, including the curly brackets!
{
  cd /app
  source scripts/utils.sh
  source scripts/review_apps/review-apps.sh
  deploy
}

# This is to remove the files that were created/modified by "the job"
git restore .
```

#### See the Helm values that will be used for the helm release

Copy the helm command at the bottom of the previous section. You have to change this command a bit before running it locally:

1. Add `--dry-run --debug` flags to the command
2. Rename the release name from `gitlab-CHANGEME` to `gitlab/gitlab`
3. Change the `--version` flag to e.g. `5.5.0` (or the helm chart version you'd like to work with)

You can now run the `helm` command in a separate, non-docker shell, as you need to have `helm` and `kubectl` commands available. You can then find the helm values under `USER-SUPPLIED VALUES:`.

#### Compare the `helm upgrade` commands between `master` and your branch

Goal: Generate a diff of what changed in the final `review-deploy` command.

```shell
git checkout master

# Do the procedure above, and copy everything under `USER-SUPPLIED VALUES:`. Save it to a file named review-deploy-master.out

git checkout <your-branch>
# Do the procedure above, and copy everything under `USER-SUPPLIED VALUES:`. Save it to a file named review-deploy-branch.out

git diff review-deploy-master.out review-deploy-branch.out
```

### Work with the Visual Review Toolbar locally

We have enabled the [visual review toolbar GitLab feature](https://docs.gitlab.com/ee/ci/review_apps/#using-visual-reviews) for review apps ([see the MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/85520)).

In order to have it working locally, you will have to do a few tweaks to your local installation:

#### Enable a feature flag to post anonymous reviews

[As explained in the feature docs](https://docs.gitlab.com/ee/ci/review_apps/#visual-reviews), enable the `anonymous_visual_review_feedback` feature flag locally:

```shell
cd <your-gitlab-project>
bundle exec rails console
Feature.enable(:anonymous_visual_review_feedback)
```

#### Add the environment variables

```shell
export REVIEW_APPS_ENABLED=true
export REVIEW_APPS_MERGE_REQUEST_IID=6
```

#### Change project destination

We'll use the `gitlab-org/gitlab-shell` project for the review apps:

```shell
# In app/helpers/tooling/visual_review_helper.rb, go from
    GITLAB_INSTANCE_URL            = 'https://gitlab.com'
    GITLAB_ORG_GITLAB_PROJECT_ID   = '278964'
    GITLAB_ORG_GITLAB_PROJECT_PATH = 'gitlab-org/gitlab'

# to
    GITLAB_INSTANCE_URL            = 'http://localhost:3000'
    GITLAB_ORG_GITLAB_PROJECT_ID   = '2'
    GITLAB_ORG_GITLAB_PROJECT_PATH = 'gitlab-org/gitlab-shell'
```
#### Make the gitlab-org/gitlab-shell public!

Go to http://localhost:3000/gitlab-org/gitlab-shell/edit#js-general-project-settings and change the visibility to `public`.

#### Restart your GitLab instance

```shell
gdk restart
```

The reviews should be posted directly to this MR -> http://localhost:3000/gitlab-org/gitlab-shell/-/merge_requests/6

### cert-manager

#### What is cert-manager?

[See their official page](https://cert-manager.io/).

In layman's term, it's responsible for creation/renewal of TLS certificates, so that we have HTTPS everywhere :D.

#### What does it do really?

It's a complex machinery, but at its core, the end result will be a TLS certificate stored in a kubernetes secret. This k8s secret can be configured to be used in an ingress with the `.tls.secretName` directive:

```shell
    tls:
    - hosts:
      - kas-review-xyz.gitlab-review.app
      secretName: <name-of-secret-that-contains-the-tls-certificate>
```

This TLS certificate will then be uploaded to a load balancer in GCP, where it's actually going to be used to encrypt traffic.

#### How do we use it in the review-apps cluster?

We are creating one wildcard certificate called `review-apps-tls`, which we use for every review apps we create.

We used to create a certificate per deployment, but [we hit a rate limitation from Let's Encrypt](https://gitlab.com/gitlab-org/gitlab/-/issues/332939).

What we now do is to [copy the wildcard TLS certificate from the `review-apps` namespace into each namespace for a review-app](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/63721). This is [the recommended way of solving this problem according to cert-manager](https://cert-manager.io/docs/tutorials/syncing-secrets-across-namespaces/). We use [Kubernetes Reflector](#reflector) for that purpose.

#### What's the difference between a certificate and a secret containing a certificate?

The certificate is a resource specific to cert-manager (it's called a Custom Resource Definition, or CRD) used to track the status of a certificate: whether it needs to be renewed, whether it's valid, ...

The secret contains the certificate itself.

#### How to upgrade cert-manager?

We have written [an upgrade guide documenting the upgrade path in the engineering-productivity-infrastructure project](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/UPGRADE.review-apps.md#cert-manager).


#### About the cert-manager pod logs

Some log entries look suspect when you first look at them, but most of them have a reason to be.

##### cert-manager pod

**ingress 'review-xyz' in work queue no longer exists**: Those namespaces have been deleted, and cert-manager still has the ingresses for that namespace registered somewhere. It then complains about it. If you find a way to tell cert-manager to unregister the certificates, please make an MR for it!
##### cert-manager-cainjector pod

**unable to fetch certificate that owns the secret**: This is due to us copy/pasting the main TLS secret in namespaces. We do not copy the certificate resource, and cert-manager complains that a secret exists without the associated certificate. There might be a better way to do this though: MRs welcome as always!
##### cert-manager-webhook pod

**TLS handshake error: EOF** I've seen this error after a migration. The "fix" is explained in [this issue](https://github.com/cert-manager/cert-manager/issues/4594): delete the pod, and the error will be gone when it recreates.

### external-dns

#### What is external-dns?

[See their official page](https://github.com/kubernetes-sigs/external-dns).

In layman's term, it's responsible for managing DNS entries.

#### How do we use it in the review-apps cluster?

When we deploy a review app, we are creating a public GCP load balancer (via nginx-ingress), so that anybody in the company can access the review-app.

We want to have a DNS name to access that review-app, as accessing by IP is less intuitive. Instead of creating a DNS entry for each review-deploy app, and deleting DNS entries when we delete a review-app, we use external-dns to manage those.

Some facts about how we use it for review apps:

* It is currently deployed via Terraform: https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/blob/main/review-apps/review_apps.tf#L196.
* It's managing a DNS zone in AWS, not GCP.
* I highly recommend reading https://gitlab.com/gitlab-org/gitlab/-/issues/383543#note_1183120433, to get an idea on how we can access the DNS records that external-dns manages in case of an outage.

#### How to query DNS records managed by external-dns?

First of all, install the `aws` command-line tool: `brew install awscli`

Then, run those commands in a shell:

```shell
# Get them from https://console.cloud.google.com/security/secret-manager?project=gitlab-review-apps
export AWS_ACCESS_KEY_ID=<redacted>
export AWS_SECRET_ACCESS_KEY=<redacted>

# To get information about the AWS account
aws sts get-caller-identity

# Query for a DNS record
aws route53 list-resource-record-sets --hosted-zone-id Z270EPS4UZGUOQ --query "ResourceRecordSets[?Name == 'gitlab-review-331432-per-3db6b1.gitlab-review.app.']"
```

you should see an A record pointing to an IP:

```shell
[
    {
        "Name": "gitlab-review-331432-per-3db6b1.gitlab-review.app.",
        "Type": "A",
        "TTL": 10,
        "ResourceRecords": [
            {
                "Value": "34.170.218.245"
            }
        ]
    },
    {
        "Name": "gitlab-review-331432-per-3db6b1.gitlab-review.app.",
        "Type": "TXT",
        "TTL": 300,
        "ResourceRecords": [
            {
                "Value": "\"heritage=external-dns,external-dns/owner=review-apps,external-dns/resource=ingress/review-331432-per-3db6b1/review-331432-per-3db6b1-webservice-default\""
            }
        ]
    }
]
```

### Reflector

#### What is reflector?

[See their official page](https://github.com/emberstack/kubernetes-reflector).

In layman's term, it's responsible to copy secrets/config maps from one source namespace to many other namespaces (called `mirrors`).

#### How do we use it in the review-apps cluster?

We use it to copy the wildcard TLS certificate used for all review-apps to a new namespace when we deploy a review-app.

#### How is it configured?

The only configuration we have are annotations on the `review-apps-tls` secret in the `review-apps` namespace:

```yaml
metadata:
  annotations:
    [...]
    reflector.v1.k8s.emberstack.com/reflection-allowed: "true"
    reflector.v1.k8s.emberstack.com/reflection-allowed-namespaces: "^(?!(?:review-apps)$)review-.*" # review-* namespaces, except the review-apps namespace
    reflector.v1.k8s.emberstack.com/reflection-auto-enabled: "true"
    reflector.v1.k8s.emberstack.com/reflection-auto-namespaces: "" # All namespaces, filtered by the `reflection-allowed-namespaces` regexp
```

This allows us to duplicate the secret in all `review-*` Kubernetes namespaces (except the `review-apps` namespace).
