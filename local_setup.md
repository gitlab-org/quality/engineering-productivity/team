# Engineering productivity - local tools

Engineering productivity team uses local developer tooling connected to specific cloud resources to be able to test and validate changes locally.

[[_TOC_]]

## zsh

Obviously, you're free to use bash or any other shell, but since zsh is the default in macOS, this guide focuses on it.

* [Install oh-my-zsh](https://github.com/ohmyzsh/ohmyzsh#basic-installation)
* [Pick a new theme](https://github.com/ohmyzsh/ohmyzsh/wiki/Themes), and change it in your `.zshrc` (I use [awesomepanda](https://github.com/ohmyzsh/ohmyzsh/wiki/Themes#awesomepanda) in the screenshot)

```shell
# Before
ZSH_THEME="robbyrussell"

# After
ZSH_THEME="awesomepanda"
```

## asdf

https://asdf-vm.com/guide/getting-started.html

## "docker"

See handbook as of [why we're not using Docker at GitLab](https://about.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop).

* Install [Rancher Desktop](https://rancherdesktop.io/)
* If you're like me, you will encounter [this problem](https://github.com/rancher-sandbox/rancher-desktop/issues/1155). If you do:
  * `ln -s /Applications/Rancher\ Desktop.app/Contents/Resources/resources/darwin/bin/nerdctl /usr/local/bin/nerdctl`
* Optional: `alias docker=nerdctl` (add to your `.*rc` file)

You now have docker CLI available!

Test it out: `docker run ubuntu echo "Hi"`

## gcloud

I didn't install `gcloud` via `asdf` for two reasons:

1. `gcloud` has an auto-upgrade feature that prompts you to upgrade bi-weekly: I don't want to do this via asdf every time.
2. In my experience, `gcloud` version differences between developers is not really an issue, unlike for `kubectl` or `terraform`

```shell
brew install --cask google-cloud-sdk

# Don't forget the autocompletion! (the instructions will pop-up at the end of the command above)
```

## Kubernetes

```shell
# Install kubectl
asdf plugin add kubectl
asdf install kubectl latest
asdf global kubectl latest

# Optional: Alias "kubectl" to just "k"
# Optional: Add shell autocompletion for kubectl -> https://kubernetes.io/docs/tasks/tools/install-kubectl-macos/#enable-shell-autocompletion

# Install kubectx
asdf plugin add kubectx
asdf install kubectx latest
asdf global kubectx latest

# Install helm
asdf plugin add helm
asdf install helm latest
asdf global helm latest
```

### Configure access to gcloud projects/k8s clusters

#### Review apps

```shell
{
  # Create a new gcloud configuration
  gcloud config configurations create review-apps
  gcloud auth login
  gcloud config set project gitlab-review-apps
  gcloud config set compute/region us-central1
  gcloud config set compute/zone us-central1-b

  # Configure access to k8s clusters in the GCP project
  gcloud container clusters get-credentials review-apps

  # Rename the k8s context for easier reference down the road
  kubectl config rename-context $(kubectl config current-context) review-apps
}
```

#### Triage-ops

```shell
{
  # Create a new gcloud configuration
  gcloud config configurations create triage-ops
  gcloud auth login
  gcloud config set project gitlab-qa-resources
  gcloud config set compute/region us-central1

  # Configure access to k8s clusters in the GCP project
  gcloud container clusters get-credentials triage-ops-prod

  # Rename the k8s context for easier reference down the road
  kubectl config rename-context $(kubectl config current-context) triage-ops
}
```

#### test-metrics

```shell
{
  # Create a new gcloud configuration
  gcloud config configurations create test-metrics
  gcloud auth login
  gcloud config set project gitlab-qa-resources
  gcloud config set compute/region us-central1

  # Configure access to k8s clusters in the GCP project
  gcloud container clusters get-credentials test-metrics

  # Rename the k8s context for easier reference down the road
  kubectl config rename-context $(kubectl config current-context) test-metrics
}
```

#### gitlab-runners

```shell
{
  # Create a new gcloud configuration
  gcloud config configurations create gitlab-runners
  gcloud auth login
  gcloud config set project gitlab-qa-resources
  gcloud config set compute/region us-central1

  # Configure access to k8s clusters in the GCP project
  gcloud container clusters get-credentials gitlab-runners

  # Rename the k8s context for easier reference down the road
  kubectl config rename-context $(kubectl config current-context) gitlab-runners
}
```

#### ci-secrets

```shell
{
  # Create a new gcloud configuration
  gcloud config configurations create ci-secrets
  gcloud auth login
  gcloud config set project gitlab-qual-ci-secret-mgmt-e78c9b95
  gcloud config set compute/region us-central1
  gcloud config set compute/zone us-central1-b
}
```

#### remote-development

```shell
{
  # Create a new gcloud configuration
  gcloud config configurations create remote-development
  gcloud auth login
  gcloud config set project gl-remote-dev-stg-0c1d2143
  gcloud config set compute/region us-central1

  # Configure access to k8s clusters in the GCP project
  gcloud container clusters get-credentials remote-development

  # Rename the k8s context for easier reference down the road
  kubectl config rename-context $(kubectl config current-context) remote-development
}
```

### Easy switch between environments

Those commands switch to the wanted gcloud/k8s environment. You can add the following script to your `.bashrc`/`.zshrc` file:

```shell
# Engineering productivity - GCP/GKE projects switcher
for work_environment in review-apps triage-ops test-metrics gitlab-runners ci-secrets remote-development; do
  eval "${work_environment}() {
    gcloud config configurations activate $work_environment
    kubectx $work_environment
  }"
done
```

You can then call the commands from a new shell!

```shell
$ review-apps
Activated [review-apps].
Switched to context "review-apps".

# gcloud is now configured to work in the review-apps project
# kubectl is now configured to work on the review-apps kubernetes cluster
```

Note that these commands change the namespace inside the `kubectl` context (thanks to `kubectx`),
and `helm` is using the namespace from the current `kubectl` context so you shouldn't have to specify the namespace in `helm` commands.

### About Kubernetes contexts

`kubectl` define [contexts](https://kubernetes.io/docs/reference/kubectl/cheatsheet/#kubectl-context-and-configuration),
which are keeping track of which Kubernetes cluster you are connecting to with which user, and which k8s namespace you were in.
We configure contexts in [the local setup instructions](#configure-access-to-gcloud-projectsk8s-clusters).
You can see what it looks like under the hood by looking at `~/.kube/config`:

```yaml
- context:
    cluster: gke_gitlab-review-apps_us-central1-b_review-apps
    namespace: review-apps
    user: gke_gitlab-review-apps_us-central1-b_review-apps
  name: review-apps
```

Note that you can also use `kubens` to change the namespace to the value you want.

## Shell prompt with gcloud/k8s (Optional)

In this section, we're using [zsh plugins](https://github.com/ohmyzsh/ohmyzsh/wiki/Plugins) to add gcloud/k8s context to our prompt. Here's the end result:

![shell-prompt.png](assets/shell-prompt.png)

### In case it gets too slow

Warning: This will make your shell fairly slow. If you are not working with GCP/Kubernetes on a particular day, you can disable the GCP/Kubectl plugins in your `.zshrc`:

```shell
# Before
plugins=(
  git
  zsh-autosuggestions
  zsh-kubectl-prompt
)

source $ZSH/oh-my-zsh.sh
source $ZSH/custom/plugins/zsh-gcloud-prompt/gcloud.zsh

# After
plugins=(
  git
  zsh-autosuggestions
  # zsh-kubectl-prompt
)

source $ZSH/oh-my-zsh.sh
# source $ZSH/custom/plugins/zsh-gcloud-prompt/gcloud.zsh
```

The day you need to work on gcp/kubectl, just uncomment those two lines above!

### Procedure

* Install the oh-my-zsh plugins:

```shell
{
  ZSH_CUSTOM_PLUGINS_PATH=$ZSH/custom/plugins

  git clone https://github.com/zsh-users/zsh-autosuggestions "${ZSH_CUSTOM_PLUGINS_PATH}"/zsh-autosuggestions
  git clone https://github.com/superbrothers/zsh-kubectl-prompt.git "${ZSH_CUSTOM_PLUGINS_PATH}"/zsh-kubectl-prompt
  git clone https://github.com/ocadaruma/zsh-gcloud-prompt.git "${ZSH_CUSTOM_PLUGINS_PATH}"/zsh-gcloud-prompt
}
```

* Source some misbehaving oh-my-zsh plugins in your `.zshrc`:

```shell
# Before
source $ZSH/oh-my-zsh.sh

# After
source $ZSH/oh-my-zsh.sh
source $ZSH/custom/plugins/zsh-gcloud-prompt/gcloud.zsh
```

* Add plugins in your `.zshrc`:

```shell
# Before
plugins=(git)

# After
plugins=(
  git
  zsh-autosuggestions
  zsh-kubectl-prompt
)
```

* Change your prompt line in your theme

```shell
code $ZSH/themes/awesomepanda.zsh-theme # Or nano/vim/...

# Before
PROMPT='${ret_status}%{$fg_bold[green]%} %{$fg[cyan]%}%c %{$fg_bold[blue]%}$(git_prompt_info)%{$fg_bold[blue]%}$(svn_prompt_info)%{$reset_color%}'

# After
PROMPT='${ret_status}%{$fg[cyan]%}%~ %{$fg_bold[blue]%}$(git_prompt_info)[gcp: %{$fg_bold[yellow]%}$ZSH_GCLOUD_PROMPT%{$reset_color%} | k8s: %{$fg_bold[yellow]%}$ZSH_KUBECTL_PROMPT%{$reset_color%}] $ '
```

* As a last step, I modified the `zsh-gcloud-prompt` script to go from `ZSH_GCLOUD_PROMPT="${account}:${project}"` to `ZSH_GCLOUD_PROMPT="${project}"`, because I don't care much about the GCP account:

```shell
code $ZSH/custom/plugins/zsh-gcloud-prompt/gcloud.zsh

# Before
function _set_zsh_gcloud_prompt() {
    local account project

    account="$(gcloud config get-value account 2>/dev/null)"
    project="$(gcloud config get-value project 2>/dev/null)"

    ZSH_GCLOUD_PROMPT="${account}:${project}"
}

# After
function _set_zsh_gcloud_prompt() {
    local project

    project="$(gcloud config get-value project 2>/dev/null)"

    ZSH_GCLOUD_PROMPT="${project}"
}
```

## Terraform

* Install terraform:

```shell
asdf plugin add terraform
asdf install terraform latest
asdf global terraform latest
```

* Setup your GCP sandbox environment (next section)
* You're now ready to follow the [infrastructure development guide](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure#development)

### Setting up your GCP sandbox environment

First of all, you will need to [create a new GCP sandbox project](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#individual-aws-account-or-gcp-project).

#### Use your GCP sandbox environment from the GCP Console

After setting up your GCP sandbox project, you should see your sandbox project in the [GCP console](https://console.cloud.google.com/) (at the top in the GCP project switcher).

#### Use your GCP sandbox environment from your terminal

If `gcloud` is configured with your GitLab account on your machine (if not, run `gcloud auth login` to authenticate), all you need to do is to let Terraform authenticate via the Google SDK with this command:

```shell
# If that command succeeds, you don't need to do the login command below
gcloud auth application-default print-access-token

# Only execute if you received an error in the command above
gcloud auth application-default login
```

From there, you need to tell `gcloud` to use your new GCP sandbox project. The cleanest way to accomplish this is via [gcloud configurations](https://cloud.google.com/sdk/gcloud/reference/config/configurations):

```shell
{
  # TODO: Set the GCP project ID of your sandbox GCP project created earlier (e.g. mine looks like `ddieulivol-a829sa910`)
  GCP_PROJECT_ID=xxx

  # Create a new gcloud configuration for your gcp sandbox
  gcloud config configurations create gcp-sandbox
  gcloud auth login
  gcloud config set project "${GCP_PROJECT_ID}"
  gcloud config set compute/region us-central1 # Could be anything else
}

# List configurations
gcloud config configurations list

# Switch to your gcp-sandbox configuration
gcloud config configurations activate gcp-sandbox

# Switch back to your default configuration
gcloud config configurations activate default
```

pro-tip: You can then add the following to your `.bashrc`/`.zshrc` to have a `gcp-sandbox` command in your shell to switch to that GCP environment :tada:

```shell
# Engineering productivity - GCP project switcher
for work_environment in gcp-sandbox; do
  eval "${work_environment}() {
    gcloud config configurations activate $work_environment
  }"
done
```

#### Using your new GCP sandbox environment with Terraform

```shell
mkdir -p my-terraform-sandbox
cd my-terraform-sandbox

cat > main.tf <<TERRAFORM
terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.90"
    }
  }
}

provider "google" {
  # TODO: REPLACE THIS WITH YOUR GCP SANDBOX PROJECT ID
  project = "<YOUR_GCP_SANDBOX_PROJECT_ID>"
  region  = "us-central1"
}

resource "google_monitoring_alert_policy" "my-resource" {
  [...]
}
TERRAFORM

terraform init
terraform plan

# If you like what you see in the plan, apply.
terraform apply
```

#### Pro-tip: Bootstrap your Terraform code with Terraformer

[Terraformer](https://github.com/GoogleCloudPlatform/terraformer) is a project that will create Terraform files from existing infrastructure in GCP.

It can be used like so:

* Create your resources manually in your sandbox in the GCP console
* Run Terraformer on the resources you'd like to have in Terraform
* Adapt the Terraform code to match your needs

Here's how to setup and use terraformer:

* Install terraformer: `brew install terraformer`
* Have a look at [the resources you'd like to fetch](https://github.com/GoogleCloudPlatform/terraformer/blob/master/docs/gcp.md)
  * You can use a whole class (e.g. `monitoring`) or elements of that class (e.g. `google_monitoring_alert_policy`)
* Execute the following commands:

```shell
# Creates a new, isolated Terraform module
mkdir -p terraformer-sandbox
cd terraformer-sandbox

# Creates the versions.tf to specify which providers to use
cat > versions.tf <<TF
terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.90"
    }
  }
}
TF

# Fetch those providers locally
terraform init

# Import the resources existing in the specified GCP project
terraformer import google --projects <your-gcp-project-id> --resources=monitoring

# Check out the `generated` folder :tada:
```

## Troubleshooting

### `kubectl` command fails with `No Auth Provider found for name "gcp"` error

If you see this error during [setting up kubectl](##Kubernetes), you need to download and use a Kubectl authentication plugin which is used for accessing Google Kubernetes Engine (GKE).

Follow below the instructions on how to set up `gke-gcloud-auth-plugin` to use with `kubectl`.

```shell
gcloud components install gke-gcloud-auth-plugin
export USE_GKE_GCLOUD_AUTH_PLUGIN=True
gcloud container clusters get-credentials <CLUSTER_NAME>
```

For more details, please visit https://cloud.google.com/blog/products/containers-kubernetes/kubectl-auth-changes-in-gke.
