#!/usr/bin/env ruby

require "gitlab"
require "gitlab-client-cache"
require "parallel"

# Create mermaid flowchart showing artifact dependencies.
#
# It's done by performing the following steps:
# - Inspect all pipeline's jobs via GitLab API
# - Results are cached via gitlab-client-cache
# - Download job traces
# - Extract matching job IDs by searching for "Downloading artifacts for"
# - Build a graph between job and downloading artifacts from dependent jobs
# - Merge parallelized jobs to reduce amount of edges (e.g. "jest X/Y" -> "jest")
# - Print MermaidJS to stdout
# - For example: https://gitlab.com/gitlab-org/gitlab/-/issues/442029#note_1781299298
#
# Usage: bin/ci_job_artifact_downloads.rb [project] [pipeline id]
#   For example: bin/ci_job_artifact_downloads.rb gitlab-org/gitlab 1177351670 | pbcopy

project, pipeline = ARGV

abort "usage: #$0 <project> <pipeline #>" unless pipeline

def clean_ansi(text)
  return unless text

  text
    .gsub(/\x1b\[[0-9;]*[mGKHF]/, "")
    .gsub!(/\r/, "\n")
end

def artifact_downloads_jobs(trace)
  trace.scan(/^Downloading artifacts for .*? \((\d+)\)/).map(&:first)
end

def each_job_with_artifacts(project, pipeline)
  warn "## Fetching jobs for ##{pipeline}"
  jobs = Gitlab.pipeline_jobs(project, pipeline).auto_paginate.to_a

  Parallel.each(jobs, in_threads: 8) do |job|
    warn "## Finished #{job.finished_at} pipeline=#{pipeline} job=#{job.id} name=#{job.name}"
    warn
    trace = clean_ansi(Gitlab.job_trace(project, job.id))
    next unless trace

    job_ids = artifact_downloads_jobs(trace)

    yield job, job_ids
  end
end

def merge_similar_jobs(jobs)
  job_by_name = {}

  jobs.each do |job|
    common_name = job.name
      # 5/12
      .gsub(%r{\s+\d+/\d+$}, "")
      # pg14
      .gsub(/ \w+\d+$/, "")
      # :[...]
      .gsub(/: \[.*$/, "")

    found = job_by_name[common_name]

    if found
      found.merge(job)
    else
      job_by_name[common_name] = Job.new(job.id, common_name, job.downloads)
    end
  end

  job_index = job_by_name.values.flat_map { |job| job.merged_jobs.map { |mj| [mj, job] } }.to_h
  job_by_name.each_value { |job| job.convert_downloads(job_index) }

  job_by_name
end

def as_mermaid(project, pipeline, jobs)
  jobs_by_name = merge_similar_jobs(jobs)

  used_jobs = jobs_by_name.values.select { |job| job.downloads.any? || jobs_by_name.values.any? { |j| j.downloads_from?(job.id) } }

  flow = used_jobs.flat_map { |job| job.as_mermaid }
  flow.concat used_jobs.flat_map { |job| job.downloads_as_mermaid }

  warn "#{flow.size} edges"

  raise "Too many edges" if flow.size >= 500

  <<~MERMAID
    ```mermaid
    ---
    title: "Artifact downloads for #{project} ##{pipeline}"
    ---
    flowchart LR
    #{flow.join("\n")}
    ```
  MERMAID
end

Job = Struct.new(:id, :name, :downloads) do
  def download_used!(count)
    @download_count ||= 0
    @download_count += count
  end

  def download_count
    @download_count || 0
  end

  def merged_jobs
    @merged_jobs || [id]
  end

  def pretty_name
    name = "#{downloads.values.sum}dls << " if downloads.values.sum > 0
    name = "#{name}#{self[:name]}"
    name = "#{name} (#{merged_jobs.size}j)" if merged_jobs.size > 1
    name = "#{name} >> #{download_count}dls" if download_count > 1
    name
  end

  def convert_downloads(job_index)
    add = Hash.new(0)

    before = downloads.values

    downloads.each do |id, count|
      other_job = job_index.fetch(id)
      other_job.download_used!(count)
      add[other_job.id] += count
    end

    after = add.values

    raise "#{before.inspect} != #{after.inspect}" unless after.sum == before.sum

    self.downloads = add
  end

  def merge(job)
    @merged_jobs ||= [id]
    @merged_jobs << job.id

    job.downloads.each do |id, count|
      downloads[id] ||= 0
      downloads[id] += 1
    end
  end

  def as_mermaid
    %(#{id}["#{pretty_name}"])
  end

  def downloads_as_mermaid
    downloads.map { |other_id, count| %(#{other_id}--->|#{count}|#{id}) }
  end

  def downloads_from?(name)
    downloads.key?(name)
  end
end

jobs = []

each_job_with_artifacts(project, pipeline) do |job, dowloads_from_job|
  jobs << Job.new(job.id, job.name, dowloads_from_job.map(&:to_i).tally)
end

puts as_mermaid(project, pipeline, jobs)
