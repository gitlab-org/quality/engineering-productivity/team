#!/usr/bin/env bash
set -euo pipefail

# `HOUSEKEEPER_GITLAB_API_TOKEN` must be defined in the project's CI/CD variables.
#
# The following variables must be defined in the job variables:
#   1. KEEP
#   2. GIT_USER_NAME
#   3. GIT_USER_EMAIL

# We override the git committer name and email to not use the default gdk/gdk@example.com from the CI image
git config --global user.name "${GIT_USER_NAME}"
git config --global user.email "${GIT_USER_EMAIL}"

# Recent QA GDK image did not contain `gitlab/.git` so we need to create it.
rm -fr .git
git init

git remote add housekeeper "https://gitlab-ci-token:${HOUSEKEEPER_GITLAB_API_TOKEN}@gitlab.com/gitlab-org/gitlab.git"
# We only need to fetch `master` and no other branches.
git fetch housekeeper master --filter=tree:0
git reset --hard housekeeper/master

# Maximum number of MRs to create
MAX_MRS=${MAX_MRS:-"1"}
# Extra flags, you can set it to `-d` to perform a dry-run
EXTRA_FLAGS=${EXTRA_FLAGS:-}

# Enable YJIT to speed up execution.
export RUBYOPT="--yjit"

cmd="bundle && bundle exec gitlab-housekeeper -k ${KEEP} --max-mrs ${MAX_MRS} ${EXTRA_FLAGS}"
echo "Running '${cmd}'"

eval "${cmd}"
