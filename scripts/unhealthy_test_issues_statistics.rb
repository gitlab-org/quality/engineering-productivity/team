#!/usr/bin/env ruby

require 'optparse'
require 'gitlab'
require 'descriptive_statistics'

class UnhealthyTestIssuesStatistics
  PROJECT_ID = 278964
  COLORS_TO_CODES = {
    red: 31,
    green: 32,
    yellow: 33,
    blue: 34,
    purple: 35,
    cyan: 36,
    white: 37,
    orange: 48
  }.freeze
  DEFAULT_MAX_ISSUES = 1000
  HEALTH_PROBLEM_DEFINITIONS = {
    flakiness: {
      labels: ['test-health:pass-after-retry'],
      reports_header_regex: /### Flakiness reports \((?<reports_count>\d+)\)/
    },
    slowness: {
      labels: ['test-health:slow'],
      reports_header_regex: /### Slowness reports \((?<reports_count>\d+)\)/
    },
    'failed-tests'.to_sym => {
      labels: ['test-health:failures'],
      excluded_labels: ['Quality', 'QA'], # They report errors in the description, not in a discussion
      reports_header_regex: /### Failure reports \((?<reports_count>\d+)\)/
    },
  }

  def initialize(options = {})
    @health_problem_data = HEALTH_PROBLEM_DEFINITIONS.fetch(options.fetch(:health_problem_type, HEALTH_PROBLEM_DEFINITIONS.keys.first).to_sym)
    @max_issues = options.fetch(:max_issues, DEFAULT_MAX_ISSUES).to_i

    @api_client = Gitlab.client(
      endpoint: 'https://gitlab.com/api/v4',
      private_token: ENV['GITLAB_API_TOKEN']
    )
  end

  def execute
    reports_counts = []

    search_options = {
      state: 'opened',
      per_page: 100,
      labels: health_problem_data.fetch(:labels)
    }

    if health_problem_data.key?(:excluded_labels)
      excluded_labels = {
        not: {
          labels: health_problem_data.fetch(:excluded_labels)
        }
      }

      search_options.merge!(excluded_labels)
    end

    api_client.issues(PROJECT_ID, search_options).auto_paginate do |issue|
      api_client.issue_notes(PROJECT_ID, issue.iid, order_by: 'updated_at', sort: 'desc').auto_paginate do |note|
        match = note['body'].match(health_problem_data.fetch(:reports_header_regex))

        next unless match

        reports_counts << match[:reports_count].to_i
        print "."

        if (reports_counts.size % (max_issues / 10)) == 0
          puts in_color("Fetched #{reports_counts.size} issues", :green)
        end

        break
      end
      break if reports_counts.size >= max_issues
    end

    puts in_color("P75: #{reports_counts.percentile(75)}", :green)
    puts in_color("P90: #{reports_counts.percentile(90)}", :yellow)
    puts in_color("P95: #{reports_counts.percentile(95)}", :orange)
    puts in_color("P99: #{reports_counts.percentile(99)}", :red)
    puts in_color(reports_counts.descriptive_statistics, :cyan)
  end

  private

  attr_reader :health_problem_data, :max_issues, :api_client

  def in_color(text, color = :green)
    "\e[#{COLORS_TO_CODES[color]}m#{text}\e[0m"
  end
end

options = {}

OptionParser.new do |opts|
  opts.on("--health-problem-type [flakiness|slowness|failed-tests]", String, "Gather statistics on flakiness,slowness,failed-tests problems") do |value|
    options[:health_problem_type] = value
  end

  opts.on("--max-issues [MAX_ISSUES]", String, "Maximum number of issues to fetch for stats") do |value|
    options[:max_issues] = value
  end

  opts.on('-h --help Displays Help') do
    puts "Usage: scripts/unhealthy_test_issues_statistics.rb [--health-problem-type flakiness|slowness|failed-tests] [--max-issues MAX_ISSUES]"
    puts
    puts "Examples:"
    puts
    puts "scripts/unhealthy_test_issues_statistics.rb --health-problem-type \"slowness\""
    puts "scripts/unhealthy_test_issues_statistics.rb --max-issues 100"

    exit
  end
end.parse!

UnhealthyTestIssuesStatistics.new(**options).execute
