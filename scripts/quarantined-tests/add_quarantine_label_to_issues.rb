#!/usr/bin/env ruby
# 
# Add the ~"quarantine" label to issues (keeps the existing labels on the issue)
#
require "gitlab"

# TODO: Fill this up with a list of issues you'd like to add the ~"quarantine" label to
#
# First, run scripts/quarantine_tests_health.rb, and take the issues in the list that don't have the ~"quarantine" label.
issue_ids = [
  424582,
  446246,
  431776,
  455603,
  449531,
  343506,
  338073,
  356772,
  472965,
  468476,
  325556,
  375028,
  469091,
  432824,
  455829,
  231426,
  345229,
  454333,
  463861,
  349762,
  326194,
  448732,
  330335,
  439529,
  350868,
  341520,
  444510,
  280554,
  196825,
  217810,
  294047,
  448337,
  444373,
  346542,
  472013,
  231426
]

issue_ids.each do |issue_id|
  issue = Gitlab.issue('gitlab-org/gitlab', issue_id)
  Gitlab.edit_issue('gitlab-org/gitlab', issue_id, labels: (issue.labels + ['quarantine']).join(','))

  puts "Added the ~\"quarantine\" label to https://gitlab.com/gitlab-org/gitlab/-/issues/#{issue_id}"
end
