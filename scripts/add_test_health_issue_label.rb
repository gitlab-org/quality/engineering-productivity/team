#!/usr/bin/env ruby

require 'optparse'
require 'gitlab'

class AddFlakinessLabels
  PROJECT_ID = 278964
  COLORS_TO_CODES = {
    red: 31,
    green: 32,
    yellow: 33,
    blue: 34,
    purple: 35,
    cyan: 36,
    white: 37
  }.freeze
  DEFAULT_MAX_ISSUES = 1000
  HealthProblemDefinition = Struct.new(:name, :labels, :status_label_regex, :reports_header_regex, keyword_init: true)
  HEALTH_PROBLEM_DEFINITIONS = {
    flakiness: HealthProblemDefinition.new(
      name: :flakiness,
      labels: ['failure::flaky-test'],
      status_label_regex: /flakiness::\d/,
      reports_header_regex: /### Flakiness reports \((?<reports_count>\d+)\)/
    ),
    slowness: HealthProblemDefinition.new(
      name: :slowness,
      labels: ['rspec:slow test'],
      status_label_regex: /slowness::\d/,
      reports_header_regex: /### Slowness reports \((?<reports_count>\d+)\)/
    )
  }

  def initialize(options = {})
    health_problem_type = options.fetch(:health_problem_type, HEALTH_PROBLEM_DEFINITIONS.keys.first).to_sym
    @health_problem_data = HEALTH_PROBLEM_DEFINITIONS.fetch(health_problem_type)
    @status_label_method_name = "#{health_problem_type}_status_label"
    @max_issues = options.fetch(:max_issues, DEFAULT_MAX_ISSUES).to_i
    @created_before = options.fetch(:created_before, nil)
    @created_after = options.fetch(:created_after, nil)
    @dry_run = !!options.fetch(:dry_run, false)
    @api_client = Gitlab.client(
      endpoint: 'https://gitlab.com/api/v4',
      private_token: ENV['GITLAB_API_TOKEN']
    )
  end

  def execute
    puts in_color('Performing a dry-run!', :blue) if dry_run

    issues_count = 0

    api_client.issues(PROJECT_ID, issues_params).auto_paginate do |issue|
      actual_status_label = issue.labels.grep(health_problem_data.status_label_regex).first
      expected_status_label = nil

      api_client.issue_notes(PROJECT_ID, issue.iid, order_by: 'created_at', sort: 'asc').auto_paginate do |note|
        match = note['body'].match(health_problem_data.reports_header_regex)
        next unless match

        issues_count += 1
        expected_status_label = __send__(status_label_method_name, match[:reports_count].to_i)

        # compare currently set label with expected one
        if actual_status_label
          if actual_status_label == expected_status_label
            puts in_color("Actual label (#{actual_status_label}) is equal to expected one for #{issue.web_url}", :green)
            break
          else
            puts in_color("Actual label (#{actual_status_label}) is not equal to expected one (#{expected_status_label}) for #{issue.web_url}", :red)
          end
        else
          puts in_color("Missing label (#{expected_status_label}) for #{issue.web_url}", :yellow)
        end

        api_client.edit_issue(PROJECT_ID, issue.iid, { add_labels: expected_status_label }) unless dry_run
        break
      end

      if actual_status_label.nil? && expected_status_label.nil?
        puts in_color("No #{health_problem_data.name} label nor any reports found for #{issue.web_url}. Defaulting to #{__send__(status_label_method_name, 1)}", :red)
        api_client.edit_issue(PROJECT_ID, issue.iid, { add_labels: __send__(status_label_method_name, 1) }) unless dry_run
      end

      break if issues_count >= max_issues
    end
  end

  private

  attr_reader :status_label_method_name, :health_problem_data, :max_issues, :created_before, :created_after, :dry_run, :api_client

  def issues_params
    {
      state: 'opened',
      order_by: 'created_at',
      sort: 'desc',
      per_page: 100,
      labels: health_problem_data.labels
    }.tap do |params|
      params[:created_before] = created_before if created_before
      params[:created_after] = created_after if created_after
    end
  end

  def in_color(text, color = :green)
    "\e[#{COLORS_TO_CODES[color]}m#{text}\e[0m"
  end

  def flakiness_status_label(reports_count)
    case reports_count
    when 399..Float::INFINITY
      'flakiness::1'
    when 37..398
      'flakiness::2'
    when 13..36
      'flakiness::3'
    else
      'flakiness::4'
    end
  end

  def slowness_status_label(reports_count)
    case reports_count
    when 40..Float::INFINITY
      'slowness::1'
    when 28..39
      'slowness::2'
    when 12..27
      'slowness::3'
    else
      'slowness::4'
    end
  end
end

options = {}

OptionParser.new do |opts|
  opts.on("--health-problem-type [flakiness|slowness]", String, "Add status label to issues for flakiness or slowness") do |value|
    options[:health_problem_type] = value
  end

  opts.on("--max-issues [MAX_ISSUES]", String, "Maximum number of issues to add labels to") do |value|
    options[:max_issues] = value
  end

  opts.on("--created-before [DATE]", String, "Filter issues created before a date") do |value|
    options[:created_before] = Time.parse(value).iso8601
  end

  opts.on("--created-after [DATE]", String, "Filter issues created after a date") do |value|
    options[:created_after] = Time.parse(value).iso8601
  end

  opts.on("--dry-run", [TrueClass, FalseClass], "Performs a dry-run") do |value|
    options[:dry_run] = value
  end

  opts.on('-h --help Displays Help') do
    puts "Usage: scripts/add_test_health_issue_label.rb [--health-problem-type flakiness|slowness] [--max-issues MAX_ISSUES]"
    puts
    puts "Examples:"
    puts
    puts "scripts/add_test_health_issue_label.rb --health-problem-type \"slowness\""
    puts "scripts/add_test_health_issue_label.rb --max-issues 100"
    puts "scripts/add_test_health_issue_label.rb --max-issues 100 --dry-run"

    exit
  end
end.parse!

AddFlakinessLabels.new(**options).execute
