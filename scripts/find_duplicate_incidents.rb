require 'gitlab'
require 'date'

api_client = Gitlab.client(
  endpoint: 'https://gitlab.com/api/v4',
  private_token: ENV['GITLAB_API_TOKEN']
)

date = "01:00pm  01/01/2023"
created_after = DateTime.parse(date)

issues = api_client.issues(
  40549124,
  state: 'closed',
  per_page: 100,
  created_after: created_after
).auto_paginate

puts 'Issues created since Jan 1 2023:'
puts issues.size

filtered_issues = issues.select do |issue|
  issue.labels.include?('master-broken::flaky-test') &&
    issue['_links']['closed_as_duplicate_of']
end

puts 'Duplicates:'
puts filtered_issues.size
