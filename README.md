# EP Team project

- [Runbooks](runbooks/index.md)
- [Monitoring](monitoring.md)
- [Learning](learning.md)
- [Local tooling setup](local_setup.md)
- [Personal Productivity tips](personal_productivity_tips.md)
- [Onboarding template](.gitlab/issue_templates/Onboarding.md)
- [Communications](comms/) and related [Merge Requests](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/merge_requests?scope=all&state=all&label_name%5B%5D=Communication)

## Scripts

We have a few useful scripts in [the scripts folder](scripts).

After cloning this project:

1. Run `bundle install` in the cloned directory
2. (Optional) Set symlinks in a folder on your path (e.g. `/usr/local/bin/`)

```shell
sudo ln -s ~/src/team/scripts/* /usr/local/bin/
```
