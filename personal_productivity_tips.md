# Personal Productivity Tips

[[_TOC_]]

## Slack keyword notifications

Consider adding [Slack keyword notifications](https://slack.com/help/articles/201355156-Configure-your-Slack-notifications#keyword-notifications). Below are examples from the team:

**Rémy**

`“rymai, rémy, coutable, tooling, danger”`

**David**

`dieulivol, Engineering Productivity, Eng Prod, EP Team, tooling, danger, review app, review apps`

**Peter**

`splattael, Peter Leitzen, Leitzen, pleitzen, rubocop, cop, cops, haml-lint, lint, lints, offense, offence, offenses, offences, engprod, engineering productivity, danger, tooling, master broken, broken master, review app, review apps`

Note: `review app` will NOT cover `review apps` as I previously expected.

## GitLab labels subscriptions

Consider [subscribing to GitLab labels](https://about.gitlab.com/blog/2016/04/13/feature-highlight-subscribe-to-label/#how-to-subscribe-to-a-label) at the `gitlab-org` group level: https://gitlab.com/groups/gitlab-org/-/labels.

You can see interesting labels from the team at https://gitlab.com/groups/gitlab-org/-/labels?subscribed=&search=productivity.

## macOS keyboard shortcuts

Consider adding [macOS keyboard shortcuts](https://www.techradar.com/how-to/computing/apple/how-to-use-text-shortcuts-on-mac-1308652). Below are some examples from Rémy & David setup:

### Review request - `rr`

```
Could you please review this MR? Thanks in advance :slight_smile: 
/assign_reviewer @ 
```

### Review done - `rd`

```
Thanks for the great work, I left some notes. :pray_tone2:
/submit_review
/remove_reviewer me
```

### Looks good to me - `lg`

```
Looks good to me, thanks! :heart: :yellow_heart: :green_heart: :purple_heart:
/approve
/milestone %"16.0" 
```

### Default metadata - `ra`

```
/milestone %16.0
/assign me

/label ~"Engineering Productivity"`
/label ~"type::feature"
/label ~"type::bug"
/label ~"type::maintenance"
```
