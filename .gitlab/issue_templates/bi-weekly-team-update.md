| [KPIs](https://handbook.gitlab.com/handbook/engineering/infrastructure/performance-indicators/) | current | target | average |
| --- | ------- | ------ | ------- |
| 🟢🟡🔴 [Master Pipeline Stability](https://handbook.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#master-pipeline-stability) | X% | Y% | Z% |
| 🟢🟡🔴 [Merge request pipeline duration](https://handbook.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#merge-request-pipeline-duration) (minutes) | current | target | average |

| [Team OKRs](#) | progress | health |
| --- | -------- | ------ |
| 🟢🟡🔴 [OKR](#) | X% | on track / needs attention / at risk |
| 🟢🟡🔴 [OKR](#) | X% | on track / needs attention / at risk |
| 🟢🟡🔴 [OKR](#) | X% | on track / needs attention / at risk |

| [PIs](https://about.gitlab.com/handbook/engineering/infrastructure-quality/performance-indicators) | current | target | average |
| --- | ------- | ------ | ------- |
| 🟢🟡🔴 [Review App deployment success rate](https://handbook.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#review-app-deployment-success-rate) | X% | Y% | Z% (3 months average) | 
| 🟢🟡🔴 [Time to First Failure P80](https://handbook.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#time-to-first-failure-p80) (minutes) | X | < Y | Z (3 months average) |
| 🟢🟡🔴 [Time to First Failure](https://handbook.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#time-to-first-failure) (minutes) | X | < X | Z (3 months average) |

##### Notes

* [note]
* [note]

### Current Projects
- [ ] 
- [ ] 

### Future Projects
- [ ] 
- [ ] 
