<!-- in following the [EP Onboarding Process](https://handbook.gitlab.com/handbook/engineering/infrastructure-quality/#engaging-with-the-quality-teams), please check off each item one-by-one as you complete them -->

Welcome to the Engineering Productivity (aka. EP) team! We‘re excited to have you! 🎉

After completing your first week, you’re going to spend increasingly more
time on your team-specific onboarding. This issue acts as a guide to
getting you ready to contribute and collaborate with the team! :collaboration:

> The quality of your onboarding determines the quality of your work.

This issue is structured similarly to your general onboarding issue, so
be sure to check out the tasks in the **New Team Member** sections.
Unlike your first issue though, this one doesn’t have must-do and
compliance tasks (🔴) and instead recommends important tasks (✨) that
aren’t subject to audit but likely critical to your contributions at
GitLab.

Otherwise, the tasks in this issue really embrace your self-leadership
skills (read more about [being a Manager of One][manager-of-one] in the
handbook). Complete as many or as few tasks in whichever order as you
find helpful to your own learning journey. Feel free to contribute
to our [onboarding template][template].

[manager-of-one]: https://handbook.gitlab.com/handbook/leadership/#managers-of-one
[template]: https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/.gitlab/issue_templates/Onboarding.md

### First Week

<details>
<summary>Hiring Manager</summary>

* [ ] Manager: Add the new team member to Geekbot for the Quality weekly standup.
* [ ] Manager: Announce the new team member in the [#quality](https://gitlab.slack.com/archives/C3JJET4Q6) slack channel.
* [ ] Manager: Add the new team member to the corresponding [Team Board](https://handbook.gitlab.com/handbook/engineering/quality/#team-boards)

</details>

----

### Month 1

<details>
<summary>New Team Member</summary>

#### Quality Onboarding

* [ ] ✨ Complete your [Onboarding task](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues)
* [ ] If you will be working in noisy environments, consider installing an application like [krisp.ai](https://krisp.ai). It filters background noise captured on your microphone to make your audio more intelligible to others in online meetings.
* [ ] Read about the [5 Gears](https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/441) approach to better manage your calendar
* [ ] Finish up your first Merge Request given to you by the Quality Engineer, if applicable.
* [ ] ✨ Search for the access-request issue titled "/Your Name/ /Your Role/ Entitlements" created by Employment Gitlab Bot for the below mentioned accounts/environments in the [access-requests](https://gitlab.com/gitlab-com/team-member-epics/access-requests) project. Ensure you have access to them:
  * [ ] Gitlab-QA 1Password Team Vault
  * [ ] [GitLab Dev](https://dev.gitlab.org) environment
  * [ ] [GitLab Staging](https://staging.gitlab.com) environment. If you don't have access, add a comment to your access-request issue mentioned above and `@mention` the person assigned to ask for assistance. Or you can open a new issue in the same project with the appropriate template.
  * [ ] GCP `gitlab-demos`, `gitlab-internal`, `gitlab-qa-resources`, and `gitlab-review-apps` projects via the [Google Cloud console](https://console.cloud.google.com/). Confirm with your manager, your assigned GCP IAM role and permissions
* [ ] ✨ [Request a license](https://handbook.gitlab.com/handbook/engineering/developer-onboarding/#working-on-gitlab-ee-developer-licenses) for you to use with GDK and other local GitLab instances when working on EE tests. In the Super Support Form, request the following:
  * [ ] License level: Ultimate
  * [ ] Number of seats, true up values, and previous user count: keep the default
  * [ ] Expiration date: 1 year from now
* [ ] ✨ Ask your onboarding buddy to add you to the [`gitlab-org/quality`](https://ops.gitlab.net/gitlab-org/quality) group as a maintainer in ops.gitlab.net environment.
* [ ] Plan to watch the [secure coding training videos](https://handbook.gitlab.com/handbook/engineering/security/secure-coding-training.html). Note that they were recorded over two days. It is suggested you break this up by topic and/or by hour over the next couple weeks. They cover secure coding practices in general and also cover security risks and mitigations for Ruby on Rails applications.
  * [ ] Recommended topics
    * [ ] [Introduction to Application Security](https://youtu.be/PXR8PTojHmc?t=273)
    * [ ] [Injection](https://youtu.be/PXR8PTojHmc?t=3123)
    * [ ] [Input validation](https://youtu.be/2VFavqfDS6w?t=7490)
    * [ ] [DevOps Best Practices](https://youtu.be/2VFavqfDS6w?t=11670)
    * [ ] [Safe client side JSON handling](https://youtu.be/2VFavqfDS6w?t=6331)
    * [ ] [Access control](https://youtu.be/bJYUxKn88so?t=268)
    * [ ] [Cookie Options and Security](https://youtu.be/bJYUxKn88so?t=9225)
    * [ ] [Authentication Best Practices](https://youtu.be/8tP2KVKHO7A?t=340)
    * [ ] [Rails 6 Security Features](https://youtu.be/8tP2KVKHO7A?t=8595)
* [ ] ✨ Understand the difference between [a section, a stage, a group and categories](https://handbook.gitlab.com/handbook/product/categories/#hierarchy).
* [ ] ✨ Read the relevant documentation and group page linked in the [devops stages](https://handbook.gitlab.com/handbook/product/categories/#devops-stages) page.
  * [ ] This page is extremely important to understand how we work at GitLab. Bookmark this page: you will most probably come to it often.
* [ ] Review the [Product Development Flow](https://handbook.gitlab.com/handbook/product-development-flow/)
* [ ] Review the [Product Development Timeline](https://handbook.gitlab.com/handbook/engineering/workflow/#product-development-timeline)
* [ ] Review the release process for [GitLab.com (SaaS)](https://handbook.gitlab.com/handbook/engineering/deployments-and-releases/deployments/) and [GitLab self-managed](https://handbook.gitlab.com/handbook/engineering/releases/#self-managed-releases-process)
* [ ] There is a lot of great information on the [GitLab Unfiltered YouTube channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) regarding team meetings, feature walkthroughs, AMAs, which can give you a better overview of the work being done.

##### Suggested learning path

* [ ] Learn about the different [teams](https://handbook.gitlab.com/handbook/engineering/quality/quality-engineering/#team-structure) within the Quality Department
* [ ] Add GitLab Quality team calendar mentioned in [meetings](https://handbook.gitlab.com/handbook/engineering/infrastructure-quality/#meetings) section
* [ ] GitLab's Quality is everyone's responsibility. Get familiar with [Our principles](https://handbook.gitlab.com/handbook/engineering/quality/#our-principles).
* [ ] [Subscribe to labels](https://gitlab.com/gitlab-org/gitlab/-/labels) that are relevant to your team on different GitLab projects. You will get an email notification for changes on the issue with your subscribed labels. Ask for help during your coffee chats with peer engineers if you're not sure which ones to subscribe.
* [ ] Bookmark for further reference the [Infrastructure Department Handbook](https://handbook.gitlab.com/handbook/engineering/infrastructure/)
  * [ ] [YouTube Quality Engineering Department playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpZUzlEnvlvBtf_NwShU_ot)
* [ ] Read about [End-to-end Testing](https://docs.gitlab.com/ee/development/testing_guide/end_to_end/#what-is-end-to-end-testing)
* [ ] Have an overview of the [GitLab Automated Test Suite](https://www.youtube.com/watch?v=r0ZicFxtfgI&feature=youtu.be)
  * [ ] Once overview is complete, reach out to your assigned buddy to assign 2 MRs for review for an orchestrated test and a non orchestrated test.
* [ ] Before writing end-to-end tests, learn [how to install your GDK](https://gitlab.com/gitlab-org/gitlab-development-kit#getting-started) and how to [run existing tests against your local development environment](https://gitlab.com/gitlab-org/gitlab/blob/master/qa/README.md#run-the-end-to-end-tests-in-a-local-development-environment).
* [ ] Attempt to [run specific tests](https://gitlab.com/gitlab-org/gitlab/blob/master/qa/README.md#running-specific-tests) in your local development environment.
* [ ] Understand [what, and how, tests can be run](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs/what_tests_can_be_run.md) besides on your local environment. To visualize the orchestration and architecture of the [GitLab QA](https://gitlab.com/gitlab-org/gitlab-qa) project please refer to the [diagram](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs/architecture.md). For a deeper understanding on what happens when you run `gitlab-qa` check this [how it works](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs/how_it_works.md) doc. *Important:* To run the `Test::Instance::Any` scenario against your local GDK please follow [these instructions](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs/run_qa_against_gdk.md).
* [ ] Watch the [AMA session](https://www.youtube.com/watch?v=FRZDPysTC1E&list=PL05JrBw4t0KpZUzlEnvlvBtf_NwShU_ot&index=6&t=0s) on Gitlab end-to-end testing framework to hear some common questions.
* [ ] (Optional) If you prefer to use RubyMine as your IDE, the license server is hosted here: https://gitlab.fls.jetbrains.com. Full instructions can be found [here](https://www.jetbrains.com/help/license-server-in-cloud/Activating_a_license.html). Or join slack channel [#jetbrains-ide](https://gitlab.slack.com/archives/CR08PTQ6T)
* [ ] Feel ready to start writing tests? Check the [Beginners Guide to writing end-to-end tests](https://docs.gitlab.com/ce/development/testing_guide/end_to_end/beginners_guide.html) doc.
  * [ ] Consult the [GitLab Development Style Guide](https://docs.gitlab.com/ee/development/contributing/style_guides.html) and the [End-to-end Testing Guide](https://docs.gitlab.com/ee/development/testing_guide/index.html)
  * [ ] Set up [lefthook](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/development/contributing/style_guides.md#install-lefthook) to automatically check for static analysis offenses before committing locally
* [ ] Read [Debugging Failing Tests and Test Pipelines](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/debugging-qa-test-failures/#qa-test-pipelines) to become more familiar with our processes around investigating test failures, quarantining, reporting bugs, and more.
* [ ] Hold coffee chats with counterpart EM, PM and at least one developer. Ask about validation challenges for department and how Quality can accelerate changes.
* [ ] Open at least one handbook or documentation MR to clarify something that you learned which was not in the handbook or docs.
* [ ] Add a new test/test framework enhancement.
* [ ] Suggest improvements to this onboarding guide.

#### Engineering Productivity Onboarding

* [ ] ✨ Schedule a Zoom call twice a month with other team members.
  * [ ] This can be complicated to do regularly due to timezones. If calls are harder to schedule, it's also possible to space them out in time (e.g. every month). Please discuss with the people directly to find an alternative.
  * [ ] Like with any other recurring calls, if you intend to space them out in time, consider instead replacing one of the time slots with an asynchronous update. For example, if you schedule a call every month on the first Monday, schedule an async update on the third Monday of every month, i.e. a calendar slot without a Zoom link. That way, you still update each other at least every two weeks.
* [ ] ✨ Join the relevant Slack channels:
  * [#g_engineering_productivity](https://gitlab.slack.com/archives/CMA7DQJRX)
  * [#master-broken](https://gitlab.slack.com/archives/CR6QH3D7C)
  * [#triage-automations](https://gitlab.slack.com/archives/CLCKT26JH)
  * [#review-apps-broken](https://gitlab.slack.com/archives/C011EM0RB71)
  * [#review-apps-monitoring](https://gitlab.slack.com/archives/C047J66QQ5P)
  * [#incident-management](https://gitlab.slack.com/archives/CB7P5CJS1)
* [ ] Go to [Sentry Team Settings page](https://new-sentry.gitlab.net/settings/gitlab/teams/) and join `#engineering-productivity` Sentry team to gain access to the projects for that team. To access Sentry Team Settings page, you will first need to create an account for Sentry by logging in to Okta.
* [ ] Review the [Engineering Productivity page](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/)
* [ ] Review the [CI configuration internals](https://docs.gitlab.com/ee/development/pipelines/internals.html) as an introduction to GitLab CI/CD pipelines.
* [ ] Review the [GitLab CI/CD Configuration Reference](https://docs.gitlab.com/ee/ci/yaml/)
* [ ] Review the [GitLab project CI pipeline](https://docs.gitlab.com/ee/development/pipelines.html)
* [ ] Read on caching mechanisms (we use those a lot in our CI/CD pipelines)
  * [ ] https://about.gitlab.com/blog/2022/09/12/a-visual-guide-to-gitlab-ci-caching/
  * [ ] https://docs.gitlab.com/ee/ci/caching/#good-caching-practices
  * [ ] https://docs.gitlab.com/ee/development/pipelines.html/#caching-strategy
  * [ ] https://docs.gitlab.com/ee/development/pipelines.html/#components-caching
* [ ] Review the Testing Guide for [Review Apps](https://docs.gitlab.com/ee/development/testing_guide/review_apps.html)
* [ ] Make [your GDK](https://gitlab.com/gitlab-org/gitlab-development-kit) and [triage-ops](https://gitlab.com/gitlab-org/quality/triage-ops) work locally
  * [ ] [Upgrade your GDK instance to Ultimate](https://docs.gitlab.com/ee/user/admin_area/license_file.html)
  * [ ] Follow the steps to [run triage-ops locally with a GitLab instance](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/doc/reactive/run_locally.md#running-with-a-local-gitlab-instance)
  * [ ] Verify that `triage-ops` works on your GDK instance
    * [ ] Go in a project under `gitlab-org` or `gitlab-com` group
    * [ ] Go to any MR, add a "Community contribution" label, and write `@gitlab-bot request_review` as a comment. A new comment should be posted right after your message (and you should see some logs in `triage-ops` output)
      * [ ] Remove any new labels added in the previous step when you are done.
* [ ] Create Data Access Requests
  * [ ] [Snowflake](https://handbook.gitlab.com/handbook/business-technology/data-team/platform/#warehouse-access) (with [the `snowflake_analyst` role](https://handbook.gitlab.com/handbook/business-technology/data-team/platform/#snowflake-analyst))
  * [ ] [Tableau](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/Tableau_Request.md)
* [ ] Introduction to Snowflake
  * [ ] Watch the [Snowflake training](https://docs.snowflake.com/user-guide/tutorials/snowflake-in-20minutes)
  * [ ] Review [Engineering Productivity team metric sources](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/#metrics)
* [ ] Ask the EP team (in Slack or an issue) to
  * [ ] add you to add our Snowflake dashboards as `Editor` (this needs to be done manually).
  * [ ] Add you as an `owner` to [the Engineering Productivity group](https://gitlab.com/groups/gitlab-org/quality/engineering-productivity).
* [ ] Get familiar with [our projects](https://about.gitlab.com/handbook/engineering/quality/engineering-productivity/#projects)
* [ ] Follow the [EP DevOps local setup guide](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/devops_local_setup.md#setting-up-the-sandbox-environment).
* [ ] Get familiar with [the EP infrastructure project](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure)
  * [ ] If you are not familiar with Terraform, do the [introduction to Terraform tutorial](https://www.terraform.io/intro)
  * [ ] [Make a `terraform plan` locally](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure#workflow)
* [ ] [Configure your gmail filters](https://mail.google.com/mail/u/0/#settings/filters) to remove some automated emails:
  * [ ] Matches: subject:(staging.gitlab.com sign-in from new location), Do this: Skip Inbox, Mark as read
  * [ ] Matches: from:(quality@gitlab.com) subject:(imported-project* Failed pipeline for), Do this: Skip Inbox, Mark as read
* [ ] Issue triage
  * [ ] Get familiar with [GitLab labels](https://docs.gitlab.com/ee/user/project/labels.html)
  * [ ] Review the [Issue Triage page](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/issue-triage/)
* [ ] Read through the [EP personal productivity tips](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/personal_productivity_tips.md)

</details>

----

### Month 2

<details>
<summary>Hiring Manager</summary>

* [ ] Manager: Create [an interview training issue](https://gitlab.com/gitlab-com/people-group/Training/issues) for them.
  * If the team member is in a non-management role:
    * [ ] Get familiar with [the peer interview for the Engineering Productivity position](https://gitlab.com/gitlab-com/people-group/hiring-processes/-/blob/master/Engineering/Quality/Engineering-Productivity-Engineer/4-BehavioralInterviews.md).
    * [ ] If the team member is senior level or above, they should train to perform [the technical interview for the Engineering Productivity position](https://gitlab.com/gitlab-com/people-group/hiring-processes/-/blob/master/Engineering/Quality/Engineering-Productivity-Engineer/3-TechnicalInterview.md). https://gitlab.com/gitlab-com/people-group/hiring-processes/-/issues/334

</details>

/due in 35 days
/assign `@MANAGER`
/assign `@NEWTEAMMEMBER`
