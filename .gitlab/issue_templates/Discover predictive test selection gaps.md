<!-- TODO: Set the title of the issue to "Discover predictive backend test selection gaps - YYYY-MM-DD" -->

## Goal

Follow the process outlined at https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/runbooks/predictive-test-selection.md#discover-predictive-test-selection-gaps to discover test selection gaps.

## Data

<!-- TODO: Change the X transitions and the two dates -->
I'll check **X transitions**, ranging from `YYYY-MM-DD` until `YYYY-MM-DD`:

<details><summary>CSV data from https://app.snowflake.com/ys68254/gitlab/w2NRGUe1t4ID</summary>

```csv
# TODO: Download the results as CSV and paste here
```

</details>

/milestone %Backlog
/assign me

/label ~"maintenance::test-gap"
/label ~"ep::pipeline"
/label ~"Engineering Productivity"
