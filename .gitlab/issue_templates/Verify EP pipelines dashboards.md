<!-- TODO: Set the title of the issue to "Verify EP pipeline dashboards - YYYY-MM-DD" -->

## Goal

Verify that the Engineering Productivity dashboards display accurate data.

The start of this process can be found on [this RUNBOOK entry](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/runbooks/pipelines.md#verify-ep-pipelines-dashboards).

## Resources to check

- [ ] [SQL queries behind the Pipeline duration dashboard](https://gitlab.com/gitlab-org/quality/engineering-productivity/snowflake-dashboard-sql/-/tree/main/generated/dashboards?ref_type=heads)
- [ ] [Pipeline duration dashboard](https://app.snowflake.com/ys68254/gitlab/#/ep-pipeline-durations-d4NWA2TAT)
- [ ] [Tableau pipeline duration dashboard](https://handbook.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#merge-request-pipeline-duration)

## Steps

- [ ] Review [the SQL queries](https://gitlab.com/gitlab-org/quality/engineering-productivity/snowflake-dashboard-sql/-/tree/main/generated/dashboards?ref_type=heads) for each widget in the dashboards.
- [ ] For pipeline dashboards **with types**, check that the CI/CD job names we use to define the types are **still used in the CI/CD config**.
  - [ ] If they aren't used anymore, check when was the last time they were used (use `git blame` and the like to find out), and:
    - [ ] If the CI/CD job name wasn't used for **more than 1 year**, remove them, and create MRs to change the following:
      - [ ] SQL queries in <https://gitlab.com/gitlab-org/quality/engineering-productivity/snowflake-dashboard-sql>
      - [ ] [The `scripts/pipelines-select` script](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/scripts/pipelines-select) in the `team` project.
      - [ ] [The `scripts/pipeline/set-pipeline-name` script](https://gitlab.com/gitlab-org/gitlab/-/blob/master/scripts/pipeline/set-pipeline-name) in `gitlab-org/gitlab`.
    - [ ] If the CI/CD job name was used **less than a year ago**, add a comment in the scripts to **remove the CI/CD job references 1 year after it was removed from the CI/CD config** ([example](https://gitlab.com/gitlab-org/quality/engineering-productivity/snowflake-dashboard-sql/-/merge_requests/47/diffs#92092f37014d37eae6e60acfec40bc4668fc59d6_17_16)).
- [ ] Have all the MRs merged, and update the SQL queries in Snowflake, if applicable.
- [ ] Compare the Snowflake dashboards and the output of the `scripts/pipelines-select` scripts. They should be identical.
- [ ] Compare the Snowflake dashboards with the Tableau dashboards. They should be identical.
- [ ] Add your results under the `Results` section below.

[Here is an example](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/480) of a past verification.

## Results

### SQL queries behind the Pipeline duration dashboard

<!-- TODO: Add the MRs that you had to create if you needed to change SQL queries, and/or CI/CD job names -->

### Pipeline duration dashboard

#### Daily

<details><summary>Snowflake</summary>

<!-- TODO: Add screenshots of the Snowflake dashboards. -->

</details>

<details><summary>Ruby script</summary>

```shell
# TODO: Update to any date you'll choose for this section
$ scripts/pipelines-select --after "YYYY-MM-DD 00:00:01" --before "YYYY-MM-DD 23:59:59"
[...]

# TODO: Paste the script output.
```

</details>

-------

#### Weekly

<details><summary>Snowflake</summary>

<!-- TODO: Add screenshots of the Snowflake dashboards. -->

</details>

<details><summary>Ruby script</summary>

```shell
# TODO: Update to any date you'll choose for this section
$ scripts/pipelines-select --after "YYYY-MM-DD 00:00:01" --before "YYYY-MM-DD 23:59:59"
[...]

# TODO: Paste the script output.
```

</details>

-------

#### Monthly

<details><summary>Snowflake</summary>

<!-- TODO: Add screenshots of the Snowflake dashboards. -->

</details>

<details><summary>Ruby script</summary>

```shell
# TODO: Update to any date you'll choose for this section
$ scripts/pipelines-select --after "YYYY-MM-DD 00:00:01" --before "YYYY-MM-DD 23:59:59"
[...]

# TODO: Paste the script output.
```

</details>

-------

#### Pipelines with types dashboards

<!-- TODO: Pick a day for which to check -->

I picked `YYYY-MM-DD` to check the types dashboards on a single day.

I also copied the pipeline duration with types widgets, and ran them for the day I chose:

```sql
-- Before
AND pipelines.finished_at >= DATEADD('month', -1, CURRENT_DATE)

-- After
AND finished_day = 'YYYY-MM-DD'
```

<!-- TODO: Change those numbers to whichever you'd like -->
For the histogram, I picked **three buckets** at random: 6, 54 and 97.

<details><summary>Snowflake</summary>

<!-- TODO: Add screenshots of the Snowflake dashboards. -->

</details>

<details><summary>Ruby script</summary>

```shell
# TODO: Update to any date you'll choose for this section
$ scripts/pipelines-select --after "YYYY-MM-DD 00:00:01" --before "YYYY-MM-DD 23:59:59" --with-types
[...]

# TODO: Paste the script output.
```

</details>

### Tableau pipeline duration dashboard

<details><summary>Snowflake</summary>

<!-- TODO: Add screenshots of the Snowflake dashboards. -->

</details>

<details><summary>Tableau</summary>

<!-- TODO: Add screenshots of the Tableau dashboards. -->

</details>

/milestone %Backlog
/assign me

/label ~"maintenance::workflow"
/label ~"ep::infrastructure"
/label ~"Engineering Productivity"
